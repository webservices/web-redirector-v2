# Migration

This sections details steps to migrate the old service to the new one.

### Export data

Connect to the main database with `sqlplus` (installed on LXPLUS).
Note: when entering the password, it needs to be quoted using `"` at the beginning and the end!

```sh
$ cat export-csv.script
SET MARKUP CSV ON QUOTE ON

spool short_urls.txt
SELECT * FROM SHORT_URLS;
spool off

spool web_namespace.txt
SELECT * FROM WEB_NAMESPACE;
spool off

spool web_directories.txt
SELECT * FROM WEB_DIRECTORIES;
spool off

spool web_server.txt
SELECT * FROM WEB_SERVER;
spool off

$ sqlplus webreg@cerndb1
> @export-csv.script

for file in {short_urls,web_namespace,web_directories,web_server}.txt; do
  # remove non-csv like lines
  grep , $file > "$(basename $file .txt).csv"
done
```

Now, the various `csv` files contain the dumps of the respective tables in a comma-separated format.
The files can either be parsed directly or imported into SQLite for easier querying (make sure to use a recent version of SQLite!):

```sh
$ sqlite3 web-redirector.sqlite3
> .import --csv short_urls.csv SHORT_URL
> .import --csv web_namespace.csv WEB_NAMESPACE
> .import --csv web_directories.csv WEB_DIRECTORIES
> .import --csv web_server.csv WEB_SERVER

# There should be no error during import!

> SELECT TYPE FROM WEB_NAMESPACE GROUP BY TYPE;
ALIAS
EXCEPTION
RESERVED
VIRTUALDIR

# Note: use the `.schema` command to view the table layout
```

Show all tables in the database: `SELECT table_name FROM user_tables;`

References:
* https://dbtut.com/index.php/2020/01/20/how-to-export-data-from-sql-plus-to-csv/
* https://en.m.wikibooks.org/wiki/Oracle_Database/SQL_Cheatsheet
* https://www.sqlite.org/cli.html#importing_files_as_csv_or_other_formats
