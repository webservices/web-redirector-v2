# CERN Web Redirector

A service for providing HTTP redirections from `cern.ch/*` (including `cern.ch/go/*`) and `go.cern/*`.

[API Documentation](./app/api/README.md)

This service replaces the unmaintained [WebRedirector written in C#](https://gitlab.cern.ch/webservices/WebRedirector).

## Repository layout

* `chart` contains the Helm chart used for deploying the application
* `app` contains the source code for building various components of the application
* `app/api/README.md` contains the documentation for the API
* `app/README.md` contains instructions for application development and set up of the necessary environment
* `admin-scripts` contains procedures for administrators to update/reassign/view short URL entries

## Architecture

<!-- diagram created with asciiflow.com -->

```ditaa
                               www, www.cern.ch, cern.ch (for redirections)
                                          |
                                          |
                                          v
                                  +------------------+
                                  |     Ingress      |
                                  |                  |
                                  | (hosted on PaaS) |
                                  +----+------+------+
                                       |      |
+------------------+      handles API  |      | handles redirections
| SSO registration |      requests     |      |
|                  |                   v      v
|   default role   | authentication +-------------------------------+
|   admin role     +----------------+  server                       |
+------------------+  with roles    | (maintains local cache of DB) |
                                    | (multiple replicas)           |
                   +--------------->|                               |
                   |                +----+-------------------+------+
+----------------+ |                     |                   |
| reserved slugs +-+                     |                   |
|   ConfigMap    |                       |                   |     reads/writes
+----------------+            logs sent  |                   |     entries via
                              via syslog |                   |     submitted via
                                         |                   |     API
                                         v                   v
                +-----------------------------+         +-------------------+
                |  Vector log aggregator      |         |  Postgres DB      |
                |  (archives logs on cephfs)  |         |  (hosted on DBOD) |
                +-----------------------------+         +-------------------+
                             |
                             v
                +----------------------------+
                |  OpenSearch instance       |
                |  (keeps logs for 30 days)  |
                +----------------------------+
```

## Environments

We are currently deploying in 2 environments:
- `qa` environment: `web-redirector-v2-qa.web.cern.ch`
  - [PaaS Project](https://paas.cern.ch/k8s/cluster/projects/web-redirector-v2-qa)
  - [Application registration](https://application-portal.web.cern.ch/manage/08da7914-9b03-42d1-8781-a77008c30ae5) with `web-services-portal-admins` as an `admin group`, registered as a "public" client.
  - [DBOD instance](https://dbod.web.cern.ch/pages/instance/web_redirector_v2_qa)
  - Database name (and username): `web_redirector_v2_qa`
- `production` environments:
  - separate deployments for `cern.ch` and `go.cern`
  - [PaaS Project](https://paas.cern.ch/k8s/cluster/projects/web-redirector-v2-production) with `openshift-admins` as an `admin group`
  - [Application registration for cern.ch](https://application-portal.web.cern.ch/manage/08dad776-eb04-4c58-8a81-25818fc09f73) with `web-services-portal-admins` as an `admin group`, registered as a "public" client
  - [Application registration for go.cern](https://application-portal.web.cern.ch/manage/08dad777-f41d-4431-8097-9af89c95339d) with `web-services-portal-admins` as an `admin group`, registered as a "public" client
  - [DBOD instance](https://dbod.web.cern.ch/pages/instance/web_redirector_v2_production)
  - Database names (and usernames): `cern_ch`, `go_cern`

For the `production` environment we need to annotate the project with `paas.okd.cern.ch/block-reserved-hostnames: "false"`.

We need to allow token exchange permissions between the web redirector and the web services portal via the Application Portal. In the web redirector's application registration:
- Select `SSO Registration` and click on the lock icon.
- Grant token exchange permissions to web services portal (prod or dev depending on the case).

Each of the application registrations has two roles:
- `default`: mapped to all authenticated users, Minimum Level Of Assurance: EduGain with SIRTIFI
- `admin-role`: mapped to `web-services-portal-admins` and `openshift-admins`

For tracking the visits, we are using Matomo, a dedicated web analytics service provided by IT-PW-WA. For both `qa` and `production` environments, we are using the production Matomo instance: <https://webanalytics.web.cern.ch/>.
In order to have access to the *Matomo Tracking API*, we have created an *auth token* through the [Matomo interface](https://webanalytics.web.cern.ch) using the credentials of the `wsmatomo` service account.

The `HELM_VALUES_FILE` for each deployment is stored in [GitLab CI/CD variables](https://gitlab.cern.ch/webservices/web-redirector-v2/-/settings/ci_cd).

## How to setup an environment from scratch

- Create a `paas` project: https://webservices-portal.web.cern.ch/paas. If it's an official project, set the category to Official and after creation add `openshift-admins` as `Administrator group`.
- Request a `postgres` instance from DBOD: https://resources.web.cern.ch/resources/Manage/DbOnDemand/Resources.aspx
- Store the admin password of the instance in a Gitlab CI variable called `DBOD_ADMIN_PASSWORD` (scoped to the relevant environment).
- Create a database, user and grant permissions: `CREATE DATABASE <DB_NAME>;`, `CREATE USER <USER_NAME> WITH PASSWORD <PASSWORD>;` (use `pwgen 30` to generate an appropriate password), `GRANT CONNECT, CREATE, TEMPORARY ON DATABASE <DB_NAME> TO <USER_NAME>;`.
- Add the following line in the [`pg_hba.conf` file of the DBOD instance](https://dbod-user-guide.web.cern.ch/instance_management/configuration_files/): `host <DB_NAME> <USER_NAME> 0.0.0.0/0 md5`
- Create an Image Pull Secret following https://paas.docs.cern.ch/2._Deploy_Applications/Deploy_Docker_Image/1-deploy-private-docker/#generating-a-token-to-pull-images-from-gitlab-registry-gitlab-registrycernch.
- Create an `OPENSHIFT_TOKEN` and add it to GitLab CI vars:
```sh
# create a new service account:
oc create serviceaccount gitlab-ci-deployer
# assign appropriate permissions to the new service account:
oc policy add-role-to-user registry-editor -z gitlab-ci-deployer
oc policy add-role-to-user edit -z gitlab-ci-deployer

# extract the access token of the service account:
oc serviceaccounts get-token gitlab-ci-deployer
```

- Create a new application in the [Application Portal](https://application-portal.web.cern.ch/) and configure the corresponding SSO registration as a public client ("My application cannot store a client secret safely"). Create an additional role called `admin-role` and map it to the groups `web-services-portal-admins` and `openshift-admins`. Add the *Application Identifier* in the `$HELM_VALUES_FILE` as `env.oidcAudience`.

## Reserved slugs

The reserved site names (slugs) were stored in the legacy database.

For the new CERN Web Redirector, we extracted the reserved slugs from the legacy database into a [file](/chart/files/reserved_slugs.txt) and mounted them to the server application with a [configmap](/chart/templates/reserved-slugs.yaml).

In case a reserved site name needs to be added or removed, please update the [reserved_slugs.txt](/chart/files/reserved_slugs.txt) file and re-deploy the application.

## DNS / Loadbalancer setup

As of December 2023, we use the following setup since we need a **static IPv4/IPv6 IP address** for the `cern.ch` DNS record (so-called "Apex" records cannot use `CNAME` records, only `A` and `AAAA` are allowed).

- the `web-redirector-v2-production` project is exposed on the `apps-lb-tncrit` router shard (which uses an LBaaS instance deployed in the critical area)
  - for reference: IPv4 `188.184.77.250`, IPv6 `2001:1458:d00:3c::100:2f9`, hostnames `paas-apps-lb-tncrit.cern.ch` / `lbaas-73ae2b4e-5554-441e-b8e3-8d558b25f161.cern.ch`
- `cern.ch` has a `A` and `AAAA` record pointing directly to the IP of the LB mentioned above - this **DNS entry is managed manually** by the DNS team
- `www.cern.ch` is a `DelegatedDomainAlias` for `paas-apps-lb-tncrit.cern.ch` (see [okd-internal doc](https://okd-internal.docs.cern.ch/cluster-flavours/paas/operations/custom-domains/#provision-non-root-domains)) - this **Kubernetes resource is managed manually** (not by Helm)

References:

- [OTG0146358](https://cern.service-now.com/service-portal?id=outage&n=OTG0146358)

## Legacy import

Until December 2023 we ran a so-called "legacy import" that copied short URLs created in the old web services database to the new database.
This was necessary to support creation of WebDFS, WebAFS, Sharepoint etc. sites.
Since these site types are now deprecated, we decided to remove the legacy import functionality.
See [issue #1194](https://gitlab.cern.ch/webservices/webframeworks-planning/-/issues/1194) and [merge request !66](https://gitlab.cern.ch/webservices/web-redirector-v2/-/merge_requests/66).

## Custom alerts

We are deploying custom alerts as part of the Helm chart following <https://paas.docs.cern.ch/7._Monitoring/1-metrics/#defining-custom-alerts>.

In order to deploy the alerts, we need to set `monitoring.enabled: true` in [values.yaml](/chart/values.yaml).

## New releases

We manage new releases by creating a new `tag` from a specific `commit SHA`. The new tag can be created:

- either through the [GitLab Interface](https://gitlab.cern.ch/webservices/web-redirector-v2/-/tags/new)
- or using git

```bash
git tag -a deploy-2023-04-04 5a14f36 -m "Deployment 2023-04-04"
git push --tags origin master
```

After the creation of the tag, the tagged image is being built and the deployments to production can be manually triggered.

Note that when merging to *master*, we automatically deploy to the QA instance.

## Logs

As explained in [Environment section](#environments), `web-redirector-v2` is deployed as a normal PaaS application and does **not** have any special integrations with the OKD PaaS infrastructure (unlike for example WebEOS) to be able to easily move it to another hosting environment (e.g. Kubernetes Magnum).

In addition, we have special requirements for our logs:

* logs should be kept indefinitely
  * This requirement is carried over from the old redirector, which also stored logs forever. This turned out to be *very* useful when we had to start migrating to the a new system, since it allowed us to analyze the usage.
  * To avoid storing personally identifiable information (PII), IP addresses are obfuscated in the logs.
* logs should be structured
  * Since we have a well-known log format we can store the logs in a structured format (e.g. JSON), instead storing "raw" text log lines, which need to be parsed at a later point in time.


### File logs

HTTP Access Logs are persisted by sending them from the application via syslog to a [Vector](https://github.com/vectordotdev/vector) instance.
The Vector instance stores the logs locally (file), where they are kept indefinitely (i.e. there is no cleanup).
This means over time we need to adjust the size of the PVC (see `chart/values.yaml`) or move the logs elsewhere (to be seen).

The initial implementation uses a CephFS volume because it can be conveniently access on OKD PaaS.
Other options are writing to S3 or EOS, but this requires some form of authentication and service account.

**Note:** the default volume alarm on PaaS will alert us before the PVC runs out of space.

To access the logs, use the following command:

```bash
oc get pods -l component=log-collector
mkdir logs
oc cp $POD:/var/lib/vector logs/
ls logs/
> 2023-05-26.log.json.zst
```

The logs are compressed with [Zstandard](http://www.zstd.net/) and JSON-formatted (one entry per line). To view them on the terminal, use:

```bash
# Note: zstd tooling is available on LXPLUS and in gitlab-registry.cern.ch/paas-tools/okd4-install
zstdcat logs/2023-05-26.log.json.zst | jq .
```

The log format is as follows:

```json
{
  "bytes": 96,
  "elapsed_ms": 3.255477,
  "host": "cern.ch",
  "level": "INFO",
  "location": "http://gitlab.cern.ch/webservices/web-redirector-v2",
  "log_type": "http_access",
  "message": "Found",
  "method": "GET",
  "proto": "HTTP/1.1",
  "remote_addr": "2001:1458:201::",
  "request_id": "cern-ch-server-6578c9488d-drljm/rmnrIZv1Ll-5905156",
  "scheme": "http",
  "slug": "gitlab",
  "status_code": 302,
  "timestamp": "2023-06-14T00:00:00.041623432Z",
  "uri": "/gitlab/webservices/web-redirector-v2",
  "user_agent": "curl/7.76.1"
}
```

Note that IP addresses in the `remote_addr` field are anonymized:

* For IPv6 addresses we keep only the first 48 bits / 6 bytes - the "public topology" (<https://www.ietf.org/rfc/rfc2374.html#section-3.1>)
* For IPv4 addresses we keep only the first 16 bits / 2 bytes (recommended by Matomo, <https://matomo.org/faq/general/configure-privacy-settings-in-matomo/>)

### OpenSearch instance

Our OpenSearch tenant provides user-friendly access to the most recent logs (30 days).
A separate tenant (instead of a shared one) was requested (see [RQF2329961](https://cern.service-now.com/service-portal?id=ticket&table=u_request_fulfillment&n=RQF2329961)) so we can directly send logs to OpenSearch with Vector (using the [built-in ElasticSearch plugin](https://vector.dev/docs/reference/configuration/sinks/elasticsearch/)), instead of going through MONIT (which has a custom JSON endpoint).

Instance setup and details:

* Address <https://es-web-redirector-v2.cern.ch>
* Application registration: <https://application-portal.web.cern.ch/manage/08db6e44-206e-43f3-8ef4-933fb7186ae9>
* Admin groups (for Application Portal and OpenSearch role `all_access`): `openshift-admins` and `web-services-portal-admins`
* Go to the ["Security" page](https://es-web-redirector-v2.cern.ch/dashboards/app/security-dashboards-plugin)
* Choose `Create new role`: name "vector-log-ingestion", cluster permissions "cluster:monitor/health", index permissions "write" + "indices:admin/create" for index "vector-*"
  * *this step only needs to be performed once since the role is shared by multiple users*
* Choose `Create internal user`: username "vector_web-redirector-v2-cern-ch" (choose name as appropriate), generate a secure password with `pwgen 25`
  * the username and password need to be added to the environment-specific `HELM_VALUES_FILE`
* Go to the "vector-log-ingestion" role and assign the new user to the role ("Map users")
