# Admin scripts

This directory contains a set of scripts for performing administrative actions on the Web Redirector V2.

**Note**:The scripts have a couple of dependencies, so it is recommended to run them on LXPLUS (`ssh user@lxplus.cern.ch`) which has all dependencies pre-installed.

By default, the scripts use the QA environment.
To switch to the production cern.ch environment, use:

```
export AUDIENCE=web-redirector-v2-production-cern-ch
export ENDPOINT=web-redirector-v2-production-cern-ch.web.cern.ch
```

The user should be part of `web-services-portal-admins` or `openshift-admins` groups (see [admin-role](../README.md#Environment)).

Robustness of these scripts is not guaranteed!

## List all entries for a specific user

```
./get-shorturls-for-user.sh <ACCOUNT>
```

Note that this only shows non-deleted entries!

## Get information about an entry based on slug

```
./get-shorturls-by-slug.sh <SLUG>
```

This only works for non-deleted entries.

## Change ownership of an entry

Note: this requires that we already know the ID of the entry, which can be retrieved with the `get-shorturls-for-user.sh` script for example.

```
./change-owner.sh <ID> <NEW_OWNER>
```

To change the ownership of multiple slugs, the following commands can be used:

```
SLUGS="slug1 slug2 slug3"
NEW_OWNER="johndoe"

for slug in $SLUGS; do
  id=$(./get-shorturls-by-slug.sh "$slug" | jq -r '.[0].id')
  # this will print the new entry after the change (if successful)
  ./change-owner.sh "$id" "$NEW_OWNER"
done
```

## Change target URL of an entry

```
./change-target-url.sh <ID> <NEW_URL>
```

## Delete entry

Note: similar to the previous script, this requires that we already know the ID of the entry.

```
./delete-entry.sh <ID>
```

## Retrieve audit logs

Note: similar to the previous script, this requires that we already know the ID of the entry.

```
./get-audit-records.sh <ID>
```
