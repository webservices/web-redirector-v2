#!/usr/bin/env bash

set -e

source "$(dirname "$0")/common.sh"

SHORTURL_ID=${1}
NEW_TARGET_URL=${2}

ensure_valid_token

api_request "GET" "/shorturlentry/${SHORTURL_ID}" | jq '.'

read -p "Please confirm that you want to modify the entry above (yes/no):" confirmation
if [ "$confirmation" != "yes" ]; then
    echo "Aborting."
    exit 1
fi

api_request "PUT" "/shorturlentry/${SHORTURL_ID}" "{\"targetUrl\":\"${NEW_TARGET_URL}\"}"| jq '.'
