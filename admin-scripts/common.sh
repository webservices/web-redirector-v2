#!/usr/bin/env bash

# set -x

AUDIENCE="${AUDIENCE:-web-redirector-v2-qa}"
TOKEN_FILE="${XDG_RUNTIME_DIR:-/run/user/$(id -u)}/${AUDIENCE}.token"
ENDPOINT="${ENDPOINT:-web-redirector-v2-qa.web.cern.ch}"

decode_jwt() {
    jq -R 'split(".") | .[1] | @base64d | fromjson' <<< ${1}
}

ensure_valid_token() {
    if [ -f "$TOKEN_FILE" ]; then
        expires_at=$(decode_jwt $(<${TOKEN_FILE}) | jq -r '.exp')
        if [ $expires_at -gt $(date +%s) ]; then
            echo "Access token is valid" >&2
            return
        else
            echo "Access token is not valid" >&2
        fi
    fi

    # get a new token
    echo "Requesting new access token" >&2
    auth-get-user-token -c "$AUDIENCE"  -o "$TOKEN_FILE" -x
}

# 1: HTTP method, 2: API URL path, 3: request body (optional)
api_request() {
    curl --silent --show-error --fail \
         -H "Authorization: Bearer $(<${TOKEN_FILE})" \
         -H "Content-Type: application/json" \
         -X "$1" \
         -d "${3}" \
         "https://${ENDPOINT}/_/api${2}"
}
