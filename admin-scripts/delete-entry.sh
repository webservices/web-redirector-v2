#!/usr/bin/env bash

set -e

source "$(dirname "$0")/common.sh"

SHORTURL_ID=${1}

ensure_valid_token

api_request "GET" "/shorturlentry/${SHORTURL_ID}" | jq '.'

read -p "Please confirm that you want to delete the entry above (yes/no):" confirmation

if [ "$confirmation" != "yes" ]; then
    echo "Aborting."
    exit 1
fi

api_request "DELETE" "/shorturlentry/${SHORTURL_ID}" | jq '.'

echo "OK"
