#!/usr/bin/env bash

set -e

source "$(dirname "$0")/common.sh"

SHORTURL_ID=${1}

ensure_valid_token

api_request "GET" "/shorturlentry/${SHORTURL_ID}/audit" | jq '.'
