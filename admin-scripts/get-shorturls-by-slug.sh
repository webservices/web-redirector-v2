#!/usr/bin/env bash

set -e

source "$(dirname "$0")/common.sh"

SLUG=${1}

ensure_valid_token

api_request "GET" "/shorturlentry/?slug=${SLUG}&owner=" | jq '.'
