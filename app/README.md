# Web Redirector Application

This directory contains the source code of the web redirector application.
It is implemented in Go and provides a JSON API for creating/modifying/deleting entries.
In addition, it serves the traffic for `cern.ch/*` and `go.cern/*`.

The application consists of two binaries (see `cmd/`):
* `server` implements the main HTTP endpoints which handle redirections and API requests.
* `import` is a tool for loading the dataset from the legacy Oracle database, converting, validating and finally inserting it into the new database.

## Dependencies

This service will be maintained for a long while, hence we should avoid to introduce unnecessary dependencies, as they may become hard-to-update or entirely obsolete in the future.
Nevertheless, we don't want to re-implement everything ourselves, so we rely on a couple of libraries:

* [go-chi](https://github.com/go-chi/chi): a lightweight, fast and flexible routing framework for Go that's compatible with `net/http` handler interfaces
* [pgx](https://github.com/JackC/pgx): a fast and flexible PostgreSQL driver for Go
* [go-jwt](https://github.com/golang-jwt/jwt): validation of JWT/Oauth2 tokens
* [bbolt](https://github.com/etcd-io/bbolt): embedded key-value database used for caching
* [slog](https://pkg.go.dev/golang.org/x/exp/slog): structured logging
* [slog-syslog](https://github.com/samber/slog-syslog/): send structured logs via syslog to a remote sink

To **update a dependency** we need to use the following commands:

```sh
# Download need / updated dependecy locally:
go get <DEPENDENCY>@<RELEASE-VERSION>
# e.g. go get github.com/jackc/pgx/v5/pgxpool@v5.0.0-alpha.5

# Update vendored source files:
go mod download
go mod vendor
go mod tidy

# Commit files:
git add -f go.mod go.sum vendor/ && git commit -m "Update <DEPENDENCY> to <RELEASE-VERSION>"
```

## Development environment

The development environment should have at least `Go` 1.19 and `make` installed (please see `hack/Containerfile` for an up-to-date reference).

The `Makefile` contains several commonly-used and important function.
You can view them all with `grep ' #' Makefile | awk -F: '{print $2}'`.

To get started, first create an `env` file in the `auth/` directory (this directory is excluded from Git) with the following contents:

```
LEGACY_DB_USER=<secure>
LEGACY_DB_PASSWORD=<you-know-it>
LEGACY_DB_CONNECTION=<database>
POSTGRES_HOST=127.0.0.1
POSTGRES_PORT=5432
POSTGRES_USER=web-redirector-v2
POSTGRES_PASSWORD=<choose-a-password>
POSTGRES_DB=web-redirector-v2
DB_CONNECTION=postgresql://web-redirector-v2:<repeat-the-password>@127.0.0.1:5432/web-redirector-v2
```

For details about the available and required environment variables, refer to the README's of the binaries (under `cmd/<component>/README.md`).

Then:

```sh
# export the environment variables
set -a && . auth/env

# create a local postgres instance
make run-postgres
make migrate-postgres

# start the server:
make run-server
# try `curl -i http://localhost:3333` in another terminal

# import data from legacy database:
make run-import

# run the server again and make sure we can access the data
make run-server
```

Now we can try to create entries through the API.
For development purposes, an HTTP Header authentication mechanism has been implemented.
It simply uses the values sent in the `X-Forwarded-User` and `X-Forwarded-Roles` headers to "authenticate" the user.
In production deployments, an OIDC authentication mechanism is used, but this is too cumbersome to work with for testing and development.

```sh
curl -X GET -H 'X-Forwarded-User: jhensche' -H 'X-Forwarded-Roles: default-role,admin-role' http://localhost:3333/_/api/shorturlentry/
```

To connect to the new database, use this command (given that the environment variables have been exported):

```sh
psql "${DB_CONNECTION}"
> \dt
> \d redirections
```

## Migrations

We are using [tern](https://github.com/jackc/tern) for managing migrations. The `tern.conf` file specifies its configuration.

To create a new migration:
```sh
cd app/db/postgres/migrations/
tern new name_of_migration
```

To migrate up to the last version:
```sh
make migrate-postgres
```

To migrate to a specific version:
```sh
make migrate-postgres MIGRATION=42
```

To clear the database (undoes all migrations):
```sh
make migrate-postgres MIGRATION=0
```
