# Web Redirector V2 API

This API is mainly used by the [Webservices Portal](https://webservices-portal.web.cern.ch/), which provides a web interface for users to create, edit and delete their short URLs and web aliases.
However, users may also want to use the API programmatically.

Endpoint | Purpose | Client ID (used as the Audience for OIDC tokens)
---------|---------|-------------------------------------------------
`web-redirector-v2-qa.web.cern.ch` | please use this for testing | `web-redirector-v2-qa`
`web-redirector-v2-production-cern-ch.web.cern.ch` | responsible for `cern.ch` redirects | `web-redirector-v2-production-cern-ch`

In general, the following conventions apply to the API:

* All API calls return 401 (Unauthorized) without a valid OIDC token (token needs to be valid, signed and have the correct audience).
* All JSON API calls return 400 (Bad Request) when `Content-Type` is not `application/json` or the JSON payload in the body could not be parsed.
* In case of errors, a JSON response is returned that contains a "messages" list that holds appropriate error messages.
* A full redirection entry looks like this:
  * `id` (string)
  * `targetUrl` (string)
  * `url` (string)
  * `description` (string)
  * `owner` (string)
  * `creationTimestamp` (string)
  * `accessCount` (int)
  * `matomoId` (string)

There are two types of users (roles) for the API (these roles are defined in the corresponding application registration):

* `default-role`: users are allowed to create/modify/delete their own entries (applies to all authenticated users)
* `admin-role`: in addition to `default-role`, admin users are allowed to view/modify/delete the entries of other users

Furthermore, short URLs are *not case-sensitive*: a slug called `redirect` will respond to all casing variants, i.e. `cern.ch/redirect`, `cern.ch/Redirect`, `cern.ch/REDIRECT` etc.

## Authentication

A valid CERN SSO (OAuth2) token needs to be obtained and send in the `Authorization: Bearer ${TOKEN}` HTTP Header of each request.
The logical `owner` is extracted from the `sub` field of the JWT token.

Clients which already have an SSO registration can obtain a token using the `client_credentials` flow, see [CERN Auth docs: API Access](https://auth.docs.cern.ch/user-documentation/oidc/api-access/#registering-an-api-client).

Alternatively, human users can use the `auth-get-user-token` tool to obtain a token on the command-line:

```sh
auth-get-user-token --clientid ${AUDIENCE} --extract-token --outfile token.txt
token=$(<token.txt)
curl -X GET "https://${ENDPOINT}/_/api/shorturlentry/" -H  "Authorization: Bearer $token"
```

For details about the tool and how to install it, please refer to the [auth-get-sso-cookie repository](https://gitlab.cern.ch/authzsvc/tools/auth-get-sso-cookie).


## Short URLs

### Create

`POST /_/api/shorturlentry/`

JSON parameters:
* `targetUrl` (string): required (should only point to `.cern.ch` and `.cern` domains)
* `slug` (string): optional, sets the path of the short URL (e.g. `cern.ch/xyz123`)
* `description` (string): optional
* `appendQuery` (boolean): optional, controls if additional URL paths and query parameters should be preserved when redirecting

Returns 200 (OK) on success, 409 (Conflict) when slug already exists, 400 (Bad Request) when parameters are invalid.

JSON response:
* `full entry`

### Update

`PATCH /_/api/shorturlentry/{id}`

JSON parameters:
* `targetUrl` (string): optional
* `description` (string): optional
* `owner` (string): optional
* `matomoId` (string): optional

Returns 200 (OK) on success, 404 (Not Found) when entry does not exist, 403 (Forbidden) when user is not allowed to read/modify the entry.

JSON response:
* `full entry`

### Delete

`DELETE /_/api/shorturlentry/{id}`

Returns 200 (OK) on success or 404 (Not Found) or 403 (Forbidden) when user does not have permissions

JSON response:
* `full entry`

### List

`GET /_/api/shorturlentry/`

Query parameters:
* `owner` (string): required (only admin users are allowed to list URLs of other users)
* `slug` (string with glob matching): optional

Returns 200 (OK) on success (even if array is empty)

JSON response:
* array of `full entry`

### Get

`GET /_/api/shorturlentry/{id}`

Returns 200 (OK) on success, 404 (Not Found) or 403 (Forbidden) if user does not own the entry.

JSON response:
* `full entry`

### Generate random slug

`GET /_/api/shorturlentry/generate-slug/`

Returns 200 (OK) on success.

JSON response:
* `slug: <generated slug>`

## Audit logs

Audit logs are recorded for each modification (create/update/delete) of a short URL entry.
Admin users have access to the following endpoint to retrieve the logs for a particular redirection:

`GET /_/api/shorturlentry/{id}/audit`

Returns 200 (OK) on success, or 403 (Forbidden) if user is not an administrator.

JSON response:

* `action` (string): one of `create`, `update`, `delete`
* `request_id` (string): unique identifier for the request that triggered the action
* `source_ip` (string): IP address of the client that triggered the action
* `timestamp` (string): timestamp of the action
* `user` (string): ID of the user that triggered the action
* `data` (string): unstructured, JSON-formatted data giving more information about the action (e.g. the fields that were modified)
