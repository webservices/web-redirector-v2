package api

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"time"

	"webredirector/util"

	"github.com/MicahParks/keyfunc"
	"github.com/golang-jwt/jwt/v4"
)

// Create a struct that will be encoded to a JWT.
// Add jwt.RegisteredClaims as an embedded type, to provide fields like expiry time
type Claims struct {
	Roles []string `json:"cern_roles,omitempty"`
	jwt.RegisteredClaims
}

// Custom authentication middleware: https://go-chi.io/#/pages/middleware
// Sets the following values on the request context:
// * userId string
// * userRoles []string
func (app *App) OidcAuthMiddleware(next http.Handler) http.Handler {
	// Create the keyfunc options. Use an error handler that logs. Refresh the JWKS when a JWT signed by an unknown KID
	// is found or at the specified interval. Rate limit these refreshes. Timeout the initial JWKS refresh request after
	// 10 seconds. This timeout is also used to create the initial context.Context for keyfunc.Get.
	options := keyfunc.Options{
		RefreshErrorHandler: func(err error) {
			app.log.Error("Error from jwt.Keyfunc: %s", err)
		},
		RefreshInterval:   time.Hour,
		RefreshRateLimit:  time.Minute * 5,
		RefreshTimeout:    time.Second * 10,
		RefreshUnknownKID: true,
	}

	// Keycloak's "jwks_uri" according to https://auth.cern.ch/auth/realms/cern/.well-known/openid-configuration
	// this endpoint implements https://www.rfc-editor.org/rfc/rfc7517
	jwksUrl := fmt.Sprintf("%s/protocol/openid-connect/certs", strings.TrimSuffix(app.config.OidcIssuerUrl, "/"))
	// This fetches the key, decodes it appropriately and returns the token verification function
	jwks, err := keyfunc.Get(jwksUrl, options)
	if err != nil {
		app.log.Fatal("Failed to create JWKS from '%s': %s", jwksUrl, err)
	}

	app.log.Info("Successfully retrieved public key(s) from OIDC token issuer %s: %v", app.config.OidcIssuerUrl, jwks.KIDs())

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// extract the oauth token from the Authorization header
		tokenString := strings.TrimPrefix(r.Header.Get("Authorization"), "Bearer ")

		// Parse the token
		claims := &Claims{}
		// Parse the JWT string and store the result in `claims`.
		// Note that we are passing the key in this method as well. This method will return an error
		// if the token is invalid (if it has expired according to the expiry time we set on sign in),
		// or if the signature does not match
		token, err := jwt.ParseWithClaims(tokenString, claims, jwks.Keyfunc)
		if err != nil {
			// expectionally, here we show an error message to the client,
			// because we know that a) the error messages coming from the JWT library don't reveal any sensitive information
			// and b) because the contain useful information for the client to understand what we wrong
			// (e.g. "the access token expired")
			app.log.Info("Unable to parse JWT: %s", err)
			writeJsonError(w, http.StatusUnauthorized, fmt.Sprintf("Unable to parse JWT in Authorization header: %s", err))
			return
		}
		app.log.Debug("Decoded JWT token: %#v", token)

		// check if the token is valid
		if !token.Valid {
			app.log.Debug("Successfully decoded token from client, but it is invalid.")
			writeJsonError(w, http.StatusUnauthorized, "Unable to parse JWT in Authorization header")
			return
		}

		// ensure the token has the correct audience
		if !claims.VerifyAudience(app.config.OidcAudience, true) {
			app.log.Info("Client sent token that is valid for '%s', was expecting audience '%s'", claims.Audience[0], app.config.OidcAudience)
			writeJsonError(w, http.StatusUnauthorized, "JWT signed for unknown audience")
			return
		}

		// create new context from `r` request context
		ctx := r.Context()
		// add user id to request context
		userId := claims.Subject
		ctx = context.WithValue(ctx, "userId", userId)
		app.log.Debug("Added userId '%s' to request context", userId)
		// add user roles to request context so we can distinguish regular and privileged users later
		roles := claims.Roles
		ctx = context.WithValue(ctx, "userRoles", roles)
		app.log.Debug("Added userRoles '%s' to request context", roles)

		// call the next handler in the chain, passing the response writer and
		// the updated request object with the new context value.
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// This simple authentication middleware has the same effect as the OidcAuthMiddleware,
// but uses the `X-Forwarded-For` (username) and `X-Forwarded-Roles` headers instead.
// This should only be used for development purposes, as it allows any client (that can
// reach the API endpoint directly) to inject arbitrary usernames and passwords.
func (app *App) HeaderAuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		ctx = context.WithValue(ctx, "userId",
			r.Header.Get("X-Forwarded-User"),
		)
		ctx = context.WithValue(ctx, "userRoles",
			strings.Split(r.Header.Get("X-Forwarded-Roles"), ","),
		)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func isUserAdmin(r *http.Request) bool {
	roles := r.Context().Value("userRoles").([]string)
	return util.StringInSlice("admin-role", roles)
}
