package api

type Config struct {
	OidcAudience    string
	OidcIssuerUrl   string
	CanonicalHost   string
	MatomoServerUrl string
	MatomoAuthToken string
}
