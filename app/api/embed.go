package api

import (
	_ "embed"
)

// Use Go's "embed" module for bundling the files into the final binary
// https://godocs.io/embed

//go:embed security.txt
var securityTxt []byte

//go:embed 404-error-page.html
var notFoundHtml []byte
