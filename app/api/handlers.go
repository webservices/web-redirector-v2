package api

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"

	"webredirector/db"
	"webredirector/models"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

// This function simply redirects to home.cern
func handleHome(w http.ResponseWriter, r *http.Request) {
	const url = "https://home.cern"
	http.Redirect(w, r, url, http.StatusFound)
}

// This function returns a pretty "404 - Not Found" page
func handleNotFound(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Write(notFoundHtml)
}

// This functions returns CERN's security contact information
// https://securitytxt.org/
func handleSecurityTxt(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.Write(securityTxt)
}

// This function sets the HTTP response status and returns a JSON error page with the given error message
func writeJsonError(w http.ResponseWriter, status int, msgs ...string) {
	responseData := errorPayload{
		Status:   http.StatusText(status),
		Messages: msgs,
	}

	// If this structure gets more complex, we should consider using a proper struct for it.
	// For now, this string templating approach works just fine.
	err := writeJsonResponse(w, status, responseData)
	if err != nil {
		panic("Failed to write JSON response body:" + err.Error())
	}
}

// This function creates a JSON response body by serializing `data`,
// setting the Content-Type to application/json and sending the given HTTP response status.
// It returns any errors encountered during serialization or sending data to the io.Writer.
func writeJsonResponse(w http.ResponseWriter, status int, data any) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	enc := json.NewEncoder(w)
	return enc.Encode(data)
}

// This function checks that the Content-Type header of a given Http Request is set appropriately
// and then deserializes the content of the body into `data` - which should be a pointer!
func parseJsonPayload(w http.ResponseWriter, r *http.Request, data any) error {
	// check request header so we don't even start parsing garbage
	if r.Header.Get("Content-Type") != "application/json" {
		writeJsonError(w, http.StatusBadRequest, "Invalid Content-Type, should be application/json")
		return fmt.Errorf("Received request without Content-Type: application/json header")
	}

	// parse JSON from body
	// read a maximum 1MB (prevents DoS attacks)
	r.Body = http.MaxBytesReader(w, r.Body, 1*1024*1024)
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&data)
	if err != nil {
		writeJsonError(w, http.StatusBadRequest, "Malformed JSON request body")
		return fmt.Errorf("Received request with invalid JSON body: %s", err)
	}

	return nil
}

func (app *App) handleLegacyGo(w http.ResponseWriter, r *http.Request) {
	slug := chi.URLParam(r, "slug")

	if slug == "" {
		handleNotFound(w, r)
		return
	}

	app.log.Debug("handleLegacyGo: '%s'", slug)
	//  Append the legacy prefix to the slug
	//  Note that we preserve the casing of the slug, since old short URLs are case-sensitive
	slug = "go/" + slug
	addLogField(r, "slug", slug)

	// retrieve the RedirectEntry associated to the "slug" from the database
	entry, err := app.cache.GetRedirectBySlug(r.Context(), slug)
	// generic server error
	if err != nil {
		app.log.Error("Failed to lookup DB entry for %s: %s", slug, err)
		http.Error(w, "Internal server error: database lookup failure", http.StatusInternalServerError)
		return
	}
	// not found error
	if entry == (models.RedirectEntry{}) {
		handleNotFound(w, r)
		return
	}

	redirectUrl, err := url.Parse(entry.TargetUrl)
	if err != nil {
		app.log.Error("Encountered invalid URL in database entry '%s': %s", entry.Id, err)
		writeJsonError(w, http.StatusInternalServerError, "Internal Server Error")
		return
	}

	// if the redirection URL is protocol-relative ("protocol-less"),
	// use the protocol the request was made via (http / https)
	if redirectUrl.Scheme == "" {
		redirectUrl.Scheme = r.Header.Get("X-Forwarded-Proto")
		// if we are not running behind a proxy, the header will be empty and we just use the
		// request's protocol directly.
		if redirectUrl.Scheme == "" {
			redirectUrl.Scheme = r.URL.Scheme
		}
	}

	// serve redirection to the user
	// the Redirect() method also writes body for text clients which don't support the Location header
	http.Redirect(w, r, redirectUrl.String(), http.StatusFound)

	// update access counter and track visit in Matomo
	app.trackAccessAsync(r, &entry)
}

func (app *App) handleRedirect(w http.ResponseWriter, r *http.Request) {
	app.log.Debug("handleRedirect: %q", r.URL)

	if r.URL.Path == "" || r.URL.Path == "/" {
		handleHome(w, r)
		return
	}

	var trailingPath string
	fields := strings.SplitN(strings.TrimPrefix(r.URL.Path, "/"), "/", 2)
	// the new web redirector handles all redirection slugs case-insensitive
	var slug = strings.ToLower(fields[0])
	addLogField(r, "slug", slug)

	if slug == "" {
		handleNotFound(w, r)
		return
	}

	// only append URI path and query parameters if necessary
	if len(fields) > 1 {
		trailingPath = fields[1]
	}
	app.log.Debug("handleRedirect: slug '%s', trailing path '%s', query: %s", slug, trailingPath, r.URL.RawQuery)

	// retrieve the RedirectEntry associated to the "slug" from the cache
	entry, err := app.cache.GetRedirectBySlug(r.Context(), slug)
	// generic server error
	if err != nil {
		app.log.Error("Failed to lookup DB entry for %s: %s", slug, err)
		http.Error(w, "Internal server error: database lookup failure", http.StatusInternalServerError)
		return
	}
	// not found error
	if entry == (models.RedirectEntry{}) {
		handleNotFound(w, r)
		return
	}

	redirectUrl, err := url.Parse(entry.TargetUrl)
	if err != nil {
		app.log.Error("Encountered invalid URL in database entry '%s': %s", entry.Id, err)
		writeJsonError(w, http.StatusInternalServerError, "Internal Server Error")
		return
	}
	// if the AppendQuery flag is set, we need to append the original path and query, e.g.:
	// cern.ch/example/foo?bar=baz -> example.web.cern.ch/foo?bar=baz
	if entry.AppendQuery {
		redirectUrl = redirectUrl.JoinPath(trailingPath)
		redirectUrl.RawQuery = r.URL.RawQuery
	}

	// if the redirection URL is protocol-relative ("protocol-less"),
	// use the protocol the request was made via (http / https)
	if redirectUrl.Scheme == "" {
		app.log.Debug("No protocol specified in target URL for '%s', using request protocol.", slug)
		redirectUrl.Scheme = r.Header.Get("X-Forwarded-Proto")
		// if we are not running behind a proxy, the header will be empty
		// in this case, we just use the request's protocol directly.
		if redirectUrl.Scheme == "" {
			redirectUrl.Scheme = r.URL.Scheme
		}
	}

	// now we have all the pieces together, serve the response to the user
	// the Redirect() method also writes body for text clients which don't support the Location header
	app.log.Debug("Final RedirectURL: %s", redirectUrl.String())
	http.Redirect(w, r, redirectUrl.String(), http.StatusFound)

	// update access counter and track visit in Matomo
	app.trackAccessAsync(r, &entry)
}

// This function retrieves an entry from the database and returns its content as JSON
func (app *App) getEntry(w http.ResponseWriter, r *http.Request) {
	// get id from url
	id := chi.URLParam(r, "id")

	// retrieve entry from database
	entry := app.databaseGetEntryById(w, r, id)
	if (entry == models.RedirectEntry{}) {
		return
	}

	// check if user is allowed to get the entry
	userId := r.Context().Value("userId").(string)
	if userId == "" {
		// this condition should never be reached since the auth middleware should be in front of it
		writeJsonError(w, http.StatusUnauthorized, "User has not been authenticated")
		return
	}
	if entry.OwnerId != userId && !isUserAdmin(r) {
		writeJsonError(w, http.StatusForbidden, "User is not allowed to get this short URL")
		return
	}

	// return entry
	responseData := payloadFromRedirectEntry(entry, app.config.CanonicalHost)
	err := writeJsonResponse(w, http.StatusOK, responseData)
	if err != nil {
		app.log.Error("Failed to write JSON response body: %s", err)
		return
	}
}

// This function creates a new ShortUrlEntry in the database and returns its content as JSON
func (app *App) createEntry(w http.ResponseWriter, r *http.Request) {
	var payload entryPayload
	err := parseJsonPayload(w, r, &payload)
	if err != nil {
		app.log.Info("%s", err)
		return
	}

	// extract user info from context
	userId := r.Context().Value("userId").(string)
	if userId == "" {
		// this condition should never be reached since the auth middleware should be in front of it
		writeJsonError(w, http.StatusUnauthorized, "User has not been authenticated")
		return
	}

	app.log.Debug("createShortUrlEntry received payload from user %s: %+v", userId, payload)

	// validate payload arguments
	payload.Slug = strings.ToLower(payload.Slug)
	if payload.Slug != "" {
		// if someone is requesting a particular slug, we need to make sure it does not already exist in the DB
		app.log.Debug("User is requesting creation of %s, checking if it already exists...", payload.Slug)
		entry, err := app.db.SearchRedirects(r.Context(), db.SearchOpts{Slug: payload.Slug})
		if err == nil && len(entry) >= 1 {
			app.log.Debug("Entry '%s' already exists in database, aborting.", payload.Slug)
			writeJsonError(w, http.StatusConflict, "Entry already exists.")
			return
		}
		if err != nil {
			// we encountered a generic database error
			app.log.Error("Failed to retrieve entry %s from database: %s", payload.Slug, err)
			writeJsonError(w, http.StatusInternalServerError, "Internal Server Error")
			return
		}
	}

	// create shorturl entry (also runs validation)
	entry, errs := models.NewShortUrlEntry(strings.ToLower(payload.Slug), payload.TargetUrl, payload.Description, userId)
	// report errors (if any) to the user
	if len(errs) > 0 {
		var msg []string
		for _, err := range errs {
			msg = append(msg, err.Error())
		}
		writeJsonError(w, http.StatusBadRequest, msg...)
		return
	}
	entry.AppendQuery = payload.AppendQuery

	// store it in the database
	app.log.Debug("Insert entry into DB: %+v", entry)
	err = app.databaseStoreEntry(w, r, &entry)
	if err != nil {
		return
	}

	// record modification
	auditData, _ := json.Marshal(entry)
	app.audit(r, "create", entry.Id, auditData)

	// return URL of new entry
	responseData := payloadFromRedirectEntry(entry, app.config.CanonicalHost)
	err = writeJsonResponse(w, http.StatusCreated, responseData)
	if err != nil {
		app.log.Error("Failed to write JSON response body: %s", err)
		return
	}
}

// This function updates an exisiting entry in the database and returns its content as JSON
func (app *App) updateEntry(w http.ResponseWriter, r *http.Request) {
	// get id form url
	id := chi.URLParam(r, "id")

	var payload updatePayload
	err := parseJsonPayload(w, r, &payload)
	if err != nil {
		app.log.Info("%s", err)
		return
	}

	app.log.Debug("updateEntry received request for: %s", id)

	// retrieve entry from database
	entry := app.databaseGetEntryById(w, r, id)
	if (entry == models.RedirectEntry{}) {
		return
	}

	// check if user is allowed to update the entry
	userId := r.Context().Value("userId").(string)
	if userId == "" {
		// this condition should never be reached since the auth middleware should be in front of it
		writeJsonError(w, http.StatusUnauthorized, "User has not been authenticated")
		return
	}
	if entry.OwnerId != userId && !isUserAdmin(r) {
		writeJsonError(w, http.StatusForbidden, "User is not allowed to update this short URL")
		return
	}

	// modify the entry according to the request payload (also runs validation)
	errs := entry.Update(payload.TargetUrl, payload.Description, payload.Owner, payload.MatomoId)
	// report errors (if any) to the user
	if len(errs) > 0 {
		var msg []string
		for _, err := range errs {
			msg = append(msg, err.Error())
		}
		writeJsonError(w, http.StatusBadRequest, msg...)
		return
	}

	// update entry in database
	err = app.databaseStoreEntry(w, r, &entry)
	if err != nil {
		return
	}

	// record modification
	auditData, _ := json.Marshal(entry)
	app.audit(r, "update", entry.Id, auditData)

	// reply with the latest version of the entry
	responseData := payloadFromRedirectEntry(entry, app.config.CanonicalHost)
	err = writeJsonResponse(w, http.StatusOK, responseData)
	if err != nil {
		app.log.Error("Failed to write JSON response body: %s", err)
		return
	}
}

// This function deletes an entry from the database and returns its content as JSON
func (app *App) deleteEntry(w http.ResponseWriter, r *http.Request) {
	// get id form url
	id := chi.URLParam(r, "id")

	app.log.Debug("deleteEntry received request for: %s", id)

	// retrieve entry from database
	entry := app.databaseGetEntryById(w, r, id)
	if (entry == models.RedirectEntry{}) {
		return
	}

	// check if user is allowed to delete the entry
	userId := r.Context().Value("userId").(string)
	if userId == "" {
		// this condition should never be reached since the auth middleware should be in front of it
		writeJsonError(w, http.StatusUnauthorized, "User has not been authenticated")
		return
	}
	if entry.OwnerId != userId && !isUserAdmin(r) {
		writeJsonError(w, http.StatusForbidden, "User is not allowed to delete this short URL")
		return
	}

	// set the deletion timestamp
	entry.Delete()

	// update entry in database
	err := app.databaseStoreEntry(w, r, &entry)
	if err != nil {
		return
	}

	// record modification
	auditData, _ := json.Marshal(entry)
	app.audit(r, "delete", entry.Id, auditData)

	// reply with the latest version of the entry
	responseData := payloadFromRedirectEntry(entry, app.config.CanonicalHost)
	err = writeJsonResponse(w, http.StatusOK, responseData)
	if err != nil {
		app.log.Error("Failed to write JSON response body: %s", err)
		return
	}
}

// This function list all ShortUrlEntries the user has access to as a JSON array
func (app *App) listEntries(w http.ResponseWriter, r *http.Request) {
	// check if user is allowed to list the entries
	userId := r.Context().Value("userId").(string)
	if userId == "" {
		// this condition should never be reached since the auth middleware should be in front of it
		writeJsonError(w, http.StatusUnauthorized, "User has not been authenticated")
		return
	}

	searchOpts := db.SearchOpts{}

	ownerParam := r.URL.Query().Get("owner")
	if ownerParam != userId {
		if !isUserAdmin(r) {
			// only admins may look up entries from any user
			writeJsonError(w, http.StatusForbidden, "User is not allowed to list entries on behalf of someone else (parameter 'owner')")
			return
		}
	}
	searchOpts.OwnerId = ownerParam

	slug := r.URL.Query().Get("slug")
	if slug != "" {
		searchOpts.Slug = slug
	}

	var entries []models.RedirectEntry
	var err error
	entries, err = app.db.SearchRedirects(r.Context(), searchOpts)
	app.log.Debug("Get redirects returned %d entries", len(entries))

	if err != nil {
		app.log.Error("Failed to get entries from database %s", err)
		writeJsonError(w, http.StatusInternalServerError, "Internal Server Error")
		return
	}

	// return entries array
	responseData := payloadFromRedirectEntries(entries, app.config.CanonicalHost)
	err = writeJsonResponse(w, http.StatusOK, responseData)
	if err != nil {
		app.log.Error("Failed to write JSON response body: %s", err)
		return
	}
}

// This function list all ShortUrlEntries the user has access to as a JSON array
func (app *App) generateSlug(w http.ResponseWriter, r *http.Request) {
	// extract user info from context
	userId := r.Context().Value("userId").(string)
	if userId == "" {
		// this condition should never be reached since the auth middleware should be in front of it
		writeJsonError(w, http.StatusUnauthorized, "User has not been authenticated")
		return
	}

	slug := models.GenerateRandomSlug()

	entry, err := app.db.SearchRedirects(r.Context(), db.SearchOpts{Slug: slug})
	if err == nil && len(entry) >= 1 {
		app.log.Debug("Entry '%s' already exists in database, generating a new slug.", slug)
		slug = models.GenerateRandomSlug()
	}

	// return entry
	responseData := payloadFromSlug(slug)
	err = writeJsonResponse(w, http.StatusOK, responseData)
	if err != nil {
		app.log.Error("Failed to write JSON response body: %s", err)
		return
	}
}

func (app *App) getAuditRecords(w http.ResponseWriter, r *http.Request) {
	// get id from url
	redirectionId := chi.URLParam(r, "id")

	// only admins can view audit records
	if !isUserAdmin(r) {
		writeJsonError(w, http.StatusForbidden, "User is not allowed to view audit logs")
		return
	}

	// retrieve records from database
	records, err := app.db.GetAudits(r.Context(), redirectionId)
	app.log.Debug("Found %d audit records for redirection %s", len(records), redirectionId)
	if err != nil {
		app.log.Error("Failed to get audit records from database %s", err)
		writeJsonError(w, http.StatusInternalServerError, "Internal Server Error")
		return
	}

	// return entry
	err = writeJsonResponse(w, http.StatusOK, payloadFromAudits(records))
	if err != nil {
		app.log.Error("Failed to write JSON response body: %s", err)
		return
	}
}

// This function implements RFC 2324
func handleTeapot(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(418)
	w.Write([]byte("418 - I'm a teapot"))
}

func (app *App) databaseGetEntryById(w http.ResponseWriter, r *http.Request, id string) models.RedirectEntry {
	entries, err := app.db.SearchRedirects(r.Context(), db.SearchOpts{Id: id})
	if err != nil {
		app.log.Error("Failed to get entry from database %s", err)
		writeJsonError(w, http.StatusInternalServerError, "Internal Server Error")
		return models.RedirectEntry{}
	}
	// If len(entries) is 0, it would mean that the entry wasn't found
	if len(entries) == 0 {
		app.log.Info("Entry %s not found in database", id)
		writeJsonError(w, http.StatusNotFound, "Entry not found in database")
		return models.RedirectEntry{}
	}
	if len(entries) > 1 {
		app.log.Error("Inconsistent state detected: more than one entry returned for id '%s'", id)
		// do not continue to serve this request, since we are inconsistent state
		writeJsonError(w, http.StatusInternalServerError, "Internal Server Error")
		return models.RedirectEntry{}
	}

	return entries[0]
}

func (app *App) databaseStoreEntry(w http.ResponseWriter, r *http.Request, entry *models.RedirectEntry) error {
	err := app.db.StoreRedirect(r.Context(), entry)
	if err != nil {
		app.log.Error("Failed to store entry in database %s", err)
		writeJsonError(w, http.StatusInternalServerError, "Internal Server Error")
		return err
	}
	return nil
}

func (app *App) audit(r *http.Request, action, id string, data []byte) {
	err := app.db.StoreAudit(r.Context(), &models.AuditRecord{
		Action:          action,
		RequestId:       middleware.GetReqID(r.Context()),
		SourceIP:        r.RemoteAddr,
		User:            r.Context().Value("userId").(string),
		RedirectEntryId: id,
		Data:            data,
	})
	if err != nil {
		app.log.Error("Failed to record audit record for %s: %s", id, err)
	}
}

func (app *App) trackAccessAsync(r *http.Request, entry *models.RedirectEntry) {
	// Create a new context that will be stopped after 10 seconds.
	// This avoids leaking goroutines in the background.
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	go func(ctx context.Context) {
		app.trackAccess(ctx, r, entry)
		cancel()
	}(ctx)
}

func (app *App) trackAccess(ctx context.Context, r *http.Request, entry *models.RedirectEntry) {
	err := app.db.IncrementCounter(ctx, entry.Id)
	if err != nil {
		app.log.Error("Failed IncrementCounter for %s: %s", entry.Id, err)
	}

	shortUrl := fmt.Sprintf("%s://%s/%s", r.URL.Scheme, r.Host, entry.Slug)

	// check if the site is registered in Matomo and track the visit
	if entry.MatomoId != "" {
		app.trackVisitMatomo(ctx, r, entry, shortUrl)
	}
}

func (app *App) trackVisitMatomo(ctx context.Context, r *http.Request, entry *models.RedirectEntry, shortUrl string) {
	if app.config.MatomoAuthToken == "" {
		return
	}

	// Matomo Tracking API: https://developer.matomo.org/api-reference/tracking-api
	queryParams := url.Values{}
	queryParams.Add("idsite", entry.MatomoId)
	queryParams.Add("url", shortUrl)
	queryParams.Add("urlref", r.URL.RequestURI())
	queryParams.Add("action_name", "redirect")
	queryParams.Add("rec", "1")
	queryParams.Add("cip", r.RemoteAddr)
	queryParams.Add("token_auth", app.config.MatomoAuthToken)
	queryParams.Add("send_image", "0") // when supplying this parameter Matomo returns a "204" response (No Content)

	apiUrl, err := url.JoinPath(app.config.MatomoServerUrl, "matomo.php")
	if err != nil {
		app.log.Error("Error joining paths for Matomo API URL: %s", err)
		return
	}

	request, err := http.NewRequestWithContext(ctx, "GET", apiUrl, nil)
	if err != nil {
		app.log.Error("Failed to create request %s", err)
		return
	}

	request.URL.RawQuery = queryParams.Encode()

	resp, err := http.DefaultClient.Do(request)
	if err != nil {
		app.log.Error("Failed to track visit %s: %s", shortUrl, err)
		return
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusNoContent {
		app.log.Error("Failed to track visit for '%s': status code %d", shortUrl, resp.StatusCode)
		return
	}
	app.log.Debug("Successfully tracked access for %s in Matomo", shortUrl)
	return
}
