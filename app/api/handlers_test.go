package api

import (
	"context"
	"net/http"
	"strings"
	"testing"

	"webredirector/models"
)

func TestCreateAndDeleteShortUrlEntryWithSlug(t *testing.T) {
	app := newTestApp(t)

	// Create a new entry
	response := executeHttp(&app, httpReq{
		method:  "POST",
		path:    "/_/api/shorturlentry/",
		body:    `{"slug": "foobar123", "targetUrl": "https://new-webredirector.cern.ch/"}`,
		headers: makeHeaders("X-Forwarded-Roles: default-role"),
	})

	// Check the response (make sure our slug was respected)
	checkCode(t, response.Result(), http.StatusCreated)
	checkBodyRegex(t, response.Result(), `"url":"https:\/\/localhost:3333\/foobar123"`)
	data := decodeJsonBody(t, response)
	id := data["id"].(string)

	// Make sure the redirections works
	response = executeHttp(&app, httpReq{
		method: "GET",
		path:   "/foobar123",
	})
	checkCode(t, response.Result(), http.StatusFound)
	checkHeader(t, response.Result(), "Location", "https://new-webredirector.cern.ch/")

	// Create entry with existing slug should fail
	response = executeHttp(&app, httpReq{
		method:  "POST",
		path:    "/_/api/shorturlentry/",
		body:    `{"slug": "FOObar123", "targetUrl": "https://new-webredirector.cern.ch/"}`,
		headers: makeHeaders("X-Forwarded-Roles: default-role"),
	})

	// Check the response (make sure our slug was respected)
	checkCode(t, response.Result(), http.StatusConflict)

	// Delete the entry
	response = executeHttp(&app, httpReq{
		method:  "DELETE",
		path:    "/_/api/shorturlentry/" + id,
		headers: makeHeaders(),
	})

	// Check the response code
	checkCode(t, response.Result(), http.StatusOK)
	checkBodyRegex(t, response.Result(), `"id":"`+id+`",.+"deletionTimestamp":"20`)

	// Make sure the entry is actually deleted
	response = executeHttp(&app, httpReq{
		method: "GET",
		path:   "/foobar123",
	})
	checkCode(t, response.Result(), http.StatusNotFound)
}

func TestInvalidSlugs(t *testing.T) {
	app := newTestApp(t)

	// Users cannot create legacy "/go" short URLs
	response := executeHttp(&app, httpReq{
		method:  "POST",
		path:    "/_/api/shorturlentry/",
		body:    `{"slug": "/go/my-slug", "targetUrl": "https://webservices-portal.web.cern.ch/"}`,
		headers: makeHeaders("X-Forwarded-Roles: default-role"),
	})
	checkCode(t, response.Result(), http.StatusBadRequest)
	checkBodyRegex(t, response.Result(), `slug must consist of `)
}

func TestInvalidJSON(t *testing.T) {
	app := newTestApp(t)

	response := executeHttp(&app, httpReq{
		method:  "POST",
		path:    "/_/api/shorturlentry/",
		body:    `{this: is-not-valid-json}`,
		headers: makeHeaders(),
	})
	checkCode(t, response.Result(), http.StatusBadRequest)
	checkBodyRegex(t, response.Result(), `"messages":\[".+"\]`)
	checkHeader(t, response.Result(), "Content-Type", "application/json")

	response = executeHttp(&app, httpReq{
		method:  "POST",
		path:    "/_/api/shorturlentry/",
		body:    `{"this": "is-valid-json"}`,
		headers: makeHeaders("Content-Type: text/html"), // missing correct content-type header
	})
	checkCode(t, response.Result(), http.StatusBadRequest)
	checkBodyRegex(t, response.Result(), `"messages":\[".+"\]`)
	checkHeader(t, response.Result(), "Content-Type", "application/json")
}

func TestRedirectWithAppendQuery(t *testing.T) {
	app := newTestApp(t)

	// Create a new entry
	response := executeHttp(&app, httpReq{
		method:  "POST",
		path:    "/_/api/shorturlentry/",
		body:    `{"slug": "foobar123", "targetUrl": "http://127.0.0.1:9090/foo/bar/baz/", "appendQuery": true}`,
		headers: makeHeaders("X-Forwarded-Roles: default-role"),
	})

	// Check the response (make sure our slug was respected)
	checkCode(t, response.Result(), http.StatusCreated)
	checkBodyRegex(t, response.Result(), `"url":"https:\/\/localhost:3333\/foobar123"`)

	// Make sure the redirections works with query parameters
	response = executeHttp(&app, httpReq{
		method: "GET",
		path:   "/foobar123/?bar=baz",
	})
	checkCode(t, response.Result(), http.StatusFound)
	checkHeader(t, response.Result(), "Location", "http://127.0.0.1:9090/foo/bar/baz?bar=baz")

	// Make sure the redirections works with additional URL paths
	response = executeHttp(&app, httpReq{
		method: "GET",
		path:   "/foobar123/atlas/computing",
	})
	checkCode(t, response.Result(), http.StatusFound)
	checkHeader(t, response.Result(), "Location", "http://127.0.0.1:9090/foo/bar/baz/atlas/computing")
}

func TestCreateShortUrlEntryRandomSlug(t *testing.T) {
	app := newTestApp(t)

	// Create a new entry (we don't specify a slug, it should be auto-generated)
	response := executeHttp(&app, httpReq{
		method:  "POST",
		path:    "/_/api/shorturlentry/",
		body:    `{"targetUrl": "https://new-webredirector-007.cern.ch/"}`,
		headers: makeHeaders(),
	})

	// Check the response (make sure a random slug was generated)
	checkCode(t, response.Result(), http.StatusCreated)
	checkBodyRegex(t, response.Result(), `"url":"https:\/\/localhost:3333\/[a-z0-9]+"`)
	data := decodeJsonBody(t, response)
	url := data["url"].(string)
	fields := strings.Split(url, "/")
	slug := fields[len(fields)-1]

	// Make sure the redirections works
	response = executeHttp(&app, httpReq{
		method: "GET",
		path:   "/" + slug,
	})
	checkCode(t, response.Result(), http.StatusFound)
	checkHeader(t, response.Result(), "Location", "https://new-webredirector-007.cern.ch/")

	// Check error handling code
	response = executeHttp(&app, httpReq{
		method:  "POST",
		path:    "/_/api/shorturlentry/",
		body:    `{"targetUrl": "https:// mal-formed url"}`,
		headers: makeHeaders(),
	})
	checkCode(t, response.Result(), http.StatusBadRequest)
	checkBodyRegex(t, response.Result(), `"messages":\[".+"\]`)
}

func TestGetShortUrlEntry(t *testing.T) {
	app := newTestApp(t)

	// Create a new entry
	response := executeHttp(&app, httpReq{
		method:  "POST",
		path:    "/_/api/shorturlentry/",
		body:    `{"targetUrl": "https://new-webredirector-007.cern.ch/"}`,
		headers: makeHeaders(),
	})

	data := decodeJsonBody(t, response)
	id := data["id"].(string)

	// Execute Request
	response = executeHttp(&app, httpReq{
		method:  "GET",
		path:    "/_/api/shorturlentry/" + id,
		headers: makeHeaders(),
	})

	// Check the response code
	checkCode(t, response.Result(), http.StatusOK)
}

func TestUpdateShortUrlEntry(t *testing.T) {
	app := newTestApp(t)

	// Create a new entry
	response := executeHttp(&app, httpReq{
		method:  "POST",
		path:    "/_/api/shorturlentry/",
		body:    `{"slug": "foobar123", "targetUrl": "https://new-webredirector-007.cern.ch/"}`,
		headers: makeHeaders("X-Forwarded-Roles: default-role"),
	})

	data := decodeJsonBody(t, response)
	id := data["id"].(string)

	// Update entry
	response = executeHttp(&app, httpReq{
		method:  "PUT",
		path:    "/_/api/shorturlentry/" + id,
		body:    `{"owner": "new-owner", "targetUrl": "https://new-webredirector-008.cern.ch/", "description": "My new description", "matomoId": ""}`,
		headers: makeHeaders("X-Forwarded-Roles: default-role,admin-role"), // only admins are allowed to modify the owner
	})

	// Check the response code
	checkCode(t, response.Result(), http.StatusOK)

	// Make sure the redirection works
	response = executeHttp(&app, httpReq{
		method: "GET",
		path:   "/foobar123",
	})
	checkCode(t, response.Result(), http.StatusFound)
	checkHeader(t, response.Result(), "Location", "https://new-webredirector-008.cern.ch/")

	// Update entry with empty description
	response = executeHttp(&app, httpReq{
		method:  "PUT",
		path:    "/_/api/shorturlentry/" + id,
		body:    `{"description": ""}`,
		headers: makeHeaders("X-Forwarded-Roles: default-role,admin-role"),
	})

	// Check the response code
	checkCode(t, response.Result(), http.StatusOK)
	checkBodyRegex(t, response.Result(), `"description":""`)
}

func TestListShortUrlEntries(t *testing.T) {
	app := newTestApp(t)

	// Create a new entry
	response := executeHttp(&app, httpReq{
		method:  "POST",
		path:    "/_/api/shorturlentry/",
		body:    `{"slug": "foobar123", "targetUrl": "https://new-webredirector-007.cern.ch/"}`,
		headers: makeHeaders(),
	})

	// Get all entries for current user
	response = executeHttp(&app, httpReq{
		method:  "GET",
		path:    "/_/api/shorturlentry/?owner=test-user123",
		headers: makeHeaders(),
	})
	checkCode(t, response.Result(), http.StatusOK)
	checkBodyRegex(t, response.Result(), `\[{"`) // contains at least one element

	// Get all entries by slug
	response = executeHttp(&app, httpReq{
		method:  "GET",
		path:    "/_/api/shorturlentry/?slug=foobar123&owner=test-user123",
		headers: makeHeaders(),
	})
	checkCode(t, response.Result(), http.StatusOK)
	checkBodyRegex(t, response.Result(), `\[{"`) // contains at least one element

	// Get all entries for another user as a regular user
	response = executeHttp(&app, httpReq{
		method:  "GET",
		path:    "/_/api/shorturlentry/?owner=some-other-user",
		headers: makeHeaders(),
	})
	checkCode(t, response.Result(), http.StatusForbidden)

	// Get all entries for another user as an admin user
	response = executeHttp(&app, httpReq{
		method:  "GET",
		path:    "/_/api/shorturlentry/?owner=test-user123",
		headers: makeHeaders("X-Forwarded-User: my-admin", "X-Forwarded-Roles: default-role,admin-role"),
	})
	checkCode(t, response.Result(), http.StatusOK)
	checkBodyRegex(t, response.Result(), `\[{"`) // contains at least one element
}

func TestLegacyShortUrl(t *testing.T) {
	app := newTestApp(t)

	e := models.RedirectEntry{
		Slug:        "go/AbcDEf",
		TargetUrl:   "https://new-webredirector-go.cern.ch/",
		Description: "This is a test",
		OwnerId:     "test-user123",
	}

	ctx := context.Background()
	// Insert entry
	err := app.db.StoreRedirect(ctx, &e)
	if err != nil {
		t.Fatalf("Got error while inserting entry: %s", err)
	}

	// Make sure the redirections works
	response := executeHttp(&app, httpReq{
		method: "GET",
		path:   "/go/AbcDEf",
	})
	checkHeader(t, response.Result(), "Location", "https://new-webredirector-go.cern.ch/")

	// Ensure that legacy "Go" short URLs are handled case-sensitive
	response = executeHttp(&app, httpReq{
		method: "GET",
		path:   "/go/abcdef",
	})
	checkCode(t, response.Result(), http.StatusNotFound)

	response = executeHttp(&app, httpReq{
		method: "HEAD",
		path:   "/go/ABCDEF",
	})
	checkCode(t, response.Result(), http.StatusNotFound)

	// Ensure they also work with a trailing slash
	response = executeHttp(&app, httpReq{
		method: "HEAD",
		path:   "/go/AbcDEf/",
	})
	checkCode(t, response.Result(), http.StatusFound)

	// Ensure users can still modify existing fields
	// This tests that our validation distinguished between new slugs (which need to conform to our rules)
	// and exisiting slugs (which should always be allowed).
	response = executeHttp(&app, httpReq{
		method:  "PUT",
		path:    "/_/api/shorturlentry/" + e.Id,
		body:    `{"description": "New description", "owner": "new-owner", "targetUrl": "https://example.com/", "matomoId": ""}`,
		headers: makeHeaders("X-Forwarded-Roles: default-role"),
	})
	checkCode(t, response.Result(), http.StatusOK)
	checkBodyRegex(t, response.Result(), `"description":"New description"`)
}

func TestProtocolRelativeShortUrl(t *testing.T) {
	app := newTestApp(t)

	// Create a new entry *without* a protocol prefix
	response := executeHttp(&app, httpReq{
		method:  "POST",
		path:    "/_/api/shorturlentry/",
		body:    `{"slug": "protocol123", "targetUrl": "//protocol.example.com/"}`,
		headers: makeHeaders("X-Forwarded-Roles: default-role"),
	})

	// Test HTTP redirection without header
	response = executeHttp(&app, httpReq{
		method: "GET",
		path:   "/protocol123",
	})
	checkCode(t, response.Result(), http.StatusFound)
	checkHeader(t, response.Result(), "Location", "http://protocol.example.com/")

	// Test HTTP redirection with header
	response = executeHttp(&app, httpReq{
		method:  "GET",
		path:    "/protocol123",
		headers: makeHeaders("x-forwarded-proto: http"),
	})
	checkCode(t, response.Result(), http.StatusFound)
	checkHeader(t, response.Result(), "Location", "http://protocol.example.com/")

	// Test HTTPS redirection with header
	response = executeHttp(&app, httpReq{
		method:  "GET",
		path:    "/protocol123",
		headers: makeHeaders("X-Forwarded-Proto: https"),
	})
	checkCode(t, response.Result(), http.StatusFound)
	checkHeader(t, response.Result(), "Location", "https://protocol.example.com/")
}

func TestUrlRobustness(t *testing.T) {
	app := newTestApp(t)

	// Create a new entry
	response := executeHttp(&app, httpReq{
		method:  "POST",
		path:    "/_/api/shorturlentry/",
		body:    `{"slug": "robust42", "targetUrl": "https://robust.example.com/"}`,
		headers: makeHeaders("X-Forwarded-Roles: default-role"),
	})

	// Test regular invocation
	response = executeHttp(&app, httpReq{
		method: "GET",
		path:   "/robust42",
	})
	checkCode(t, response.Result(), http.StatusFound)
	checkHeader(t, response.Result(), "Location", "https://robust.example.com/")

	// Test case insensitivity
	response = executeHttp(&app, httpReq{
		method: "GET",
		path:   "/rObuST42",
	})
	checkCode(t, response.Result(), http.StatusFound)
	checkHeader(t, response.Result(), "Location", "https://robust.example.com/")

	// Test with multiple leading slashes
	response = executeHttp(&app, httpReq{
		method: "GET",
		path:   "///robust42",
	})
	checkCode(t, response.Result(), http.StatusFound)
	checkHeader(t, response.Result(), "Location", "https://robust.example.com/")

	// Test with trailing slashes
	response = executeHttp(&app, httpReq{
		method: "GET",
		path:   "/robust42///",
	})
	checkCode(t, response.Result(), http.StatusFound)
	checkHeader(t, response.Result(), "Location", "https://robust.example.com/")
}

func TestGenerateSlug(t *testing.T) {
	app := newTestApp(t)

	response := executeHttp(&app, httpReq{
		method:  "GET",
		path:    "/_/api/shorturlentry/generate-slug",
		headers: makeHeaders("X-Forwarded-Roles: default-role"),
	})

	checkCode(t, response.Result(), http.StatusOK)
	checkBodyRegex(t, response.Result(), `"slug":".+"`)
}

func TestSecurityTxt(t *testing.T) {
	app := newTestApp(t)

	response := executeHttp(&app, httpReq{
		method: "GET",
		path:   "/.well-known/security.txt",
	})

	checkCode(t, response.Result(), http.StatusOK)
	checkBodyRegex(t, response.Result(), `-----BEGIN PGP SIGNED MESSAGE-----`)
}

func TestAuditRecords(t *testing.T) {
	app := newTestApp(t)

	// Create a new entry
	response := executeHttp(&app, httpReq{
		method:  "POST",
		path:    "/_/api/shorturlentry/",
		body:    `{"targetUrl": "https://example123.cern.ch/"}`,
		headers: makeHeaders("X-Forwarded-User: test-for-audit-logs-85430"),
	})

	// Check the response
	checkCode(t, response.Result(), http.StatusCreated)
	data := decodeJsonBody(t, response)
	id := data["id"].(string)

	// Fetch the audit logs as regular user
	response = executeHttp(&app, httpReq{
		method:  "GET",
		path:    "/_/api/shorturlentry/" + id + "/audit",
		headers: makeHeaders("X-Forwarded-User: test-for-audit-logs-85430"),
	})
	// this should fail because we don't allow users to view audit logs
	checkCode(t, response.Result(), http.StatusForbidden)

	// Fetch audit logs as admin
	response = executeHttp(&app, httpReq{
		method:  "GET",
		path:    "/_/api/shorturlentry/" + id + "/audit",
		headers: makeHeaders("X-Forwarded-Roles: admin-role"),
	})
	checkCode(t, response.Result(), http.StatusOK)
	checkBodyRegex(t, response.Result(), `"action":"create",(.+),"user":"test-for-audit-logs-85430"`)

	// Fetch audit logs for a non-existing entry
	response = executeHttp(&app, httpReq{
		method:  "GET",
		path:    "/_/api/shorturlentry/" + "foobar-1239-348as-3249" + "/audit",
		headers: makeHeaders("X-Forwarded-Roles: admin-role"),
	})
	checkCode(t, response.Result(), http.StatusOK)
	checkBodyRegex(t, response.Result(), `^\[\]$`) // empty result []
}
