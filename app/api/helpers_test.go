package api

import (
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"regexp"
	"strings"
	"testing"

	"webredirector/cache"
	jsondb "webredirector/db/json"
	"webredirector/log"

	"github.com/go-chi/chi/v5"
)

// This file contains helper methods for running HTTP tests against our API

// executeRequest, creates a new ResponseRecorder
// then executes the request by calling ServeHTTP in the router
// after which the handler writes the response to the response recorder
// which we can then inspect.
func executeRequest(req *http.Request, app *App) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	app.router.ServeHTTP(rr, req)

	return rr
}

type httpReq struct {
	method  string
	path    string
	body    string
	headers map[string]string
}

// executeHttp is a wrapper around executeRequest that bundles the most common operations
func executeHttp(app *App, req httpReq) *httptest.ResponseRecorder {
	buf := strings.NewReader(req.body)
	request, err := http.NewRequest(req.method, "http://localhost:3333"+req.path, buf)
	if err != nil {
		panic(err)
	}
	for k, v := range req.headers {
		request.Header.Set(k, v)
	}
	return executeRequest(request, app)
}

// checkCode is a helper to compare the expected with the received HTTP response status code
func checkCode(t *testing.T, r *http.Response, expected int) {
	actual := r.StatusCode
	if expected != actual {
		t.Errorf("Expected response status code %d. Got %d\n", expected, actual)
	}
}

// checkHeader is a helper to compare an expected HTTP with received headers
func checkHeader(t *testing.T, response *http.Response, header string, expected string) {
	actual := response.Header.Get(header)
	if expected != actual {
		t.Errorf("Expected response header %s '%s'. Got '%s'\n", header, expected, actual)
	}
}

// checkBody is a helper to compare an expected HTTP body with the received one
func checkBody(t *testing.T, r *http.Response, expected string) {
	body, err := io.ReadAll(r.Body)
	if err != nil {
		t.Errorf("Failed to read response body: %s\n", err)
	}
	actual := strings.TrimSpace(string(body[:]))
	if expected != actual {
		t.Errorf("Expected response body '%s'. Got '%s'\n", expected, actual)
	}
}

// checkBodyRegex is a helper to compare an expected HTTP body with the received one
func checkBodyRegex(t *testing.T, r *http.Response, expectedRegex string) {
	body, err := io.ReadAll(r.Body)
	if err != nil {
		t.Errorf("Failed to read response body: %s\n", err)
	}
	actual := strings.TrimSpace(string(body[:]))
	regex := regexp.MustCompile(expectedRegex)
	if !regex.MatchString(actual) {
		t.Errorf("Expected response body '%s'. Got '%s'\n", expectedRegex, actual)
	}
}

// newTestApp initializes a "fake" app struct for testing with a temporary database
// just call it like this: `app := newTestApp(t)`
// Note: for testing we are using the HeaderAuthMiddleware, hence username and roles will simply
// be derived from the X-Forwarded-User and X-Forwarded-Roles headers
// See also the `makeHeaders` helper function.
func newTestApp(t *testing.T) App {
	database, err := jsondb.New("json://" + t.TempDir() + "db.json")
	if err != nil {
		t.Fatalf("Failed to initialize database: %s", err)
	}
	logger := log.NewLogger(log.LEVEL_DEBUG)

	config := Config{
		CanonicalHost: "localhost:3333",
	}

	// fake cache
	cache := cache.NewPassthrough(database)

	app := NewApp(chi.NewRouter(), database, logger, config, cache, nil)
	return app
}

// decodeJsonBody "unsafely" decodes the body as untyped JSON and returns it as a map
// type assertion needs to be used to extract the values, e.g.:
// 	var id string = data["id"].(string)
func decodeJsonBody(t *testing.T, r *httptest.ResponseRecorder) map[string]any {
	if r.Header().Get("Content-Type") != "application/json" {
		t.Errorf("Missing Content-Type: application/json header, got '%s'", r.Header().Get("Content-Type"))
	}
	data := map[string]interface{}{}
	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		t.Fatalf("Failed to decode body as JSON: %s", err)
	}

	if r.Code >= 400 {
		t.Fatalf("Server returned error message %d in JSON response: %s", r.Code, data["message"].(string))
	}
	return data
}

// makeHeaders creates the map of header that are usually required for API calls
// each header can be overriden by supplying a string, like this (note that this is case-sensitive):
// makeHeaders("Content-Type: text/html", "X-Forwarded-User: my-user")
func makeHeaders(headers ...string) map[string]string {
	// default headers
	_headers := map[string]string{
		"Host":              "localhost:3333",
		"Content-Type":      "application/json",
		"X-Forwarded-User":  "test-user123",
		"X-Forwarded-Roles": "default-role",
	}
	// overwrite headers as desired
	for _, val := range headers {
		fields := strings.SplitN(val, ":", 2)
		_headers[fields[0]] = strings.TrimSpace(fields[1])
	}
	return _headers
}
