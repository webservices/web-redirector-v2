package api

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5/middleware"
	"golang.org/x/exp/slog"
)

// StructuredHTTPLogger is an implementation of a structured logger to replace chi's default middleware logger
// It is based on https://github.com/go-chi/chi/blob/77d709fcde559ca625d4e126478835a0e85d79a9/_examples/logging/main.go

func NewStructuredLoggingMiddleware(logger *slog.Logger) func(next http.Handler) http.Handler {
	return middleware.RequestLogger(&StructuredHTTPLogger{logger})
}

// this struct implements middleware.LogFormatter interface
// https://pkg.go.dev/github.com/go-chi/chi/v5@v5.0.8/middleware#LogFormatter
type StructuredHTTPLogger struct {
	Logger *slog.Logger
}

func (l *StructuredHTTPLogger) NewLogEntry(r *http.Request) middleware.LogEntry {
	scheme := "http"
	if r.TLS != nil {
		scheme = "https"
	}
	if r.Header.Get("X-Forwarded-Proto") != "" {
		scheme = r.Header.Get("X-Forwarded-Proto")
	}
	var attrs = []slog.Attr{
		slog.String("scheme", scheme),
		slog.String("proto", r.Proto),
		slog.String("method", r.Method),
		slog.String("remote_addr", r.RemoteAddr),
		slog.String("user_agent", r.UserAgent()),
		slog.String("host", r.Host),
		slog.String("uri", r.RequestURI),
	}

	if reqID := middleware.GetReqID(r.Context()); reqID != "" {
		attrs = append(attrs, slog.String("request_id", reqID))
	}

	return &StructuredLoggerEntry{
		Logger: l.Logger,
		Attrs:  attrs,
	}
}

// this struct implements the middleware.LogEntry interface
// https://pkg.go.dev/github.com/go-chi/chi/v5@v5.0.8/middleware#LogEntry
type StructuredLoggerEntry struct {
	// Record *slog.Record
	Logger *slog.Logger
	Attrs  []slog.Attr
}

func (l *StructuredLoggerEntry) Write(status, bytes int, header http.Header, elapsed time.Duration, extra interface{}) {
	l.Attrs = append(l.Attrs,
		slog.Int("status_code", status),
		slog.Int("bytes", bytes),
		slog.Float64("elapsed_ms", float64(elapsed.Nanoseconds())/1000000.0),
	)

	if location := header.Get("Location"); location != "" {
		l.Attrs = append(l.Attrs, slog.String("location", location))
	}

	msg := http.StatusText(status)
	l.Logger.LogAttrs(context.Background(), slog.LevelInfo, msg, l.Attrs...)
}

func (l *StructuredLoggerEntry) Panic(v interface{}, stack []byte) {
	l.Attrs = append(l.Attrs,
		slog.String("stack", string(stack)),
		slog.String("panic", fmt.Sprintf("%+v", v)),
	)

	msg := http.StatusText(http.StatusInternalServerError)
	l.Logger.LogAttrs(context.Background(), slog.LevelError, msg, l.Attrs...)
}

// Helper methods that allow HTTP handlers to set additional fields as necessary, like this:
//    addLogField(r, "database_rows", 42)
func addLogField(r *http.Request, key string, value interface{}) {
	if entry, ok := r.Context().Value(middleware.LogEntryCtxKey).(*StructuredLoggerEntry); ok {
		entry.Attrs = append(entry.Attrs, slog.Any(key, value))
	}
}

// func GetLogEntry(r *http.Request) *slog.Logger {
// 	entry := middleware.GetLogEntry(r).(*StructuredLoggerEntry)
// 	return entry.Logger
// }

// func AddLogFields(r *http.Request, fields map[string]interface{}) {
// 	if entry, ok := r.Context().Value(middleware.LogEntryCtxKey).(*StructuredLoggerEntry); ok {
// 		for k, v := range fields {
// 			entry.Attrs = append(entry.Attrs, slog.Attr{Key: k, Value: v})
// 		}
// 	}
// }
