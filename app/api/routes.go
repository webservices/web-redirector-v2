package api

import (
	"net/http"
	"path"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"golang.org/x/exp/slog"

	"webredirector/cache"
	"webredirector/db"
	"webredirector/log"
)

type App struct {
	router *chi.Mux
	db     db.DB
	cache  cache.Cache
	log    log.Logger
	config Config
}

func NewApp(router *chi.Mux, database db.DB, logger log.Logger, config Config, cache cache.Cache, httpLogger *slog.Logger) App {
	app := App{}

	if database == nil {
		panic("No database implementation provided, aborting.")
	}
	app.db = database
	app.log = logger
	app.config = config
	if cache == nil {
		panic("No cache implementation provided, aborting.")

	}
	app.cache = cache

	app.router = router
	if app.router == nil {
		app.router = chi.NewRouter()
	}

	var authMiddleware func(http.Handler) http.Handler
	if app.config.OidcAudience != "" {
		authMiddleware = app.OidcAuthMiddleware
	} else {
		app.log.Warn("Using HTTP header authentication - do not use in production!")
		authMiddleware = app.HeaderAuthMiddleware
	}
	// if no logger given, fallback to stdout
	if httpLogger == nil {
		app.log.Info("No HTTP logger provided, falling back to stdout")
		httpLogger = log.NewStructuredStdoutLogger()
	}
	app.MountRoutes(authMiddleware, httpLogger)

	return app
}

// mountRoutes adds the HTTP router to the given chi router
func (app *App) MountRoutes(authMiddleware func(http.Handler) http.Handler, logger *slog.Logger) {
	r := app.router
	// add endpoint for basic healthchecks
	r.Use(middleware.Heartbeat("/_/ping"))
	// inject unique id into each request
	r.Use(middleware.RequestID)
	// ensure that http.Request's RemoteAddr is set to X-Forwarded-For / X-Real-IP header
	r.Use(middleware.RealIP)
	// remove double slashes from the URI
	r.Use(cleanPathMiddleware)
	// add logging middleware
	r.Use(NewStructuredLoggingMiddleware(logger))

	// Set a timeout value on the request context (ctx), that will signal
	// through ctx.Done() that the request has timed out and further
	// processing should be stopped.
	r.Use(middleware.Timeout(10 * time.Second))

	// Implement RFC2324
	r.Get("/_/teapot", handleTeapot)

	// Set up a handler for unknown routes
	r.NotFound(handleNotFound)

	// Handle well-known URLs
	r.Get("/.well-known/security.txt", handleSecurityTxt)

	// API subrouter (requires authentication)
	r.Group(func(r chi.Router) {
		r.Use(authMiddleware)
		r.Route("/_/api", func(r chi.Router) {
			r.Route("/shorturlentry", func(r chi.Router) {
				r.Route("/generate-slug", func(r chi.Router) {
					r.Get("/", app.generateSlug)
				})
				r.Route("/{id}", func(r chi.Router) {
					r.Get("/", app.getEntry)
					r.Put("/", app.updateEntry) // deprecated, remove after updating webservices portal
					r.Patch("/", app.updateEntry)
					r.Delete("/", app.deleteEntry)
					r.Get("/audit", app.getAuditRecords)
				})
				r.Get("/", app.listEntries)
				r.Post("/", app.createEntry)
			})
		})
	})

	r.Route("/go", func(r chi.Router) {
		r.Get("/{slug}", app.handleLegacyGo)
		r.Head("/{slug}", app.handleLegacyGo)
		r.Get("/{slug}/", app.handleLegacyGo)
		r.Head("/{slug}/", app.handleLegacyGo)
	})

	// every route not specified until now should be handled by our redirection logic
	r.Get("/*", app.handleRedirect)
	r.Head("/*", app.handleRedirect)
	r.Post("/*", app.handleRedirect)

	// generic handler for `OPTIONS` requests
	// advertises the HTTP methods implemented by this server
	r.Options("/*", func(w http.ResponseWriter, r *http.Request) {
		const allowedOptions = "OPTIONS, GET, HEAD, POST, PATCH, DELETE"
		w.Header().Set("Allow", allowedOptions)
		w.WriteHeader(200)
	})
}

func (a *App) GetRootHandler() http.Handler {
	return a.router
}

// The CleanPath middleware cleans out double slash mistakes from a user's request path, , see https://godocs.io/path#Clean
// adapted from https://github.com/go-chi/chi/blob/58ca6d6119ed77f8c1d564bac109fc36db10e3d0/middleware/clean_path.go
func cleanPathMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		rctx := chi.RouteContext(r.Context())

		routePath := rctx.RoutePath
		if routePath != "" {
			rctx.RoutePath = path.Clean(rctx.RoutePath)
		}

		if r.URL.Path != "" {
			r.URL.Path = path.Clean(r.URL.Path)
		}

		// Note that r.URL.RawPath is not modified

		next.ServeHTTP(w, r)
	})
}
