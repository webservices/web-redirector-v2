package api

import (
	"net/http"
	"testing"
)

func TestHome(t *testing.T) {
	app := newTestApp(t)

	// Create a New Request
	req, _ := http.NewRequest("GET", "/", nil)

	// Execute Request
	response := executeRequest(req, &app)

	// Check the response code
	checkCode(t, response.Result(), http.StatusFound)
	checkBody(t, response.Result(), `<a href="https://home.cern">Found</a>.`)
	checkHeader(t, response.Result(), "Location", "https://home.cern")
}

func TestNotFound(t *testing.T) {
	app := newTestApp(t)

	req, _ := http.NewRequest("GET", "/foobar123", nil)
	response := executeRequest(req, &app)
	checkCode(t, response.Result(), http.StatusNotFound)
	checkBodyRegex(t, response.Result(), "Page Not Found")

	req, _ = http.NewRequest("GET", "/_/api/this-does-not-exist", nil)
	response = executeRequest(req, &app)
	checkCode(t, response.Result(), http.StatusNotFound)
	checkBodyRegex(t, response.Result(), "Page Not Found")
}

func TestRFC2324(t *testing.T) {
	app := newTestApp(t)

	// Create a New Request
	req, _ := http.NewRequest("GET", "/_/teapot", nil)

	// Execute Request
	response := executeRequest(req, &app)

	// Check the response code
	checkCode(t, response.Result(), http.StatusTeapot)
}

func TestOptions(t *testing.T) {
	app := newTestApp(t)

	req, _ := http.NewRequest("OPTIONS", "/", nil)
	response := executeRequest(req, &app)
	checkCode(t, response.Result(), http.StatusOK)
	checkHeader(t, response.Result(), "Allow", "OPTIONS, GET, HEAD, POST, PATCH, DELETE")
}
