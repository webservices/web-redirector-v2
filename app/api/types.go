package api

import (
	"fmt"
	"time"
	"webredirector/models"
)

// omitempty isn't being used since we want to return all the
// attributes even if the value is empty.
type entryPayload struct {
	Id                string    `json:"id"`
	Slug              string    `json:"slug"`
	Url               string    `json:"url"`
	TargetUrl         string    `json:"targetUrl"`
	AppendQuery       bool      `json:"appendQuery"`
	Description       string    `json:"description"`
	CreationTimestamp time.Time `json:"creationTimestamp"`
	DeletionTimestamp time.Time `json:"deletionTimestamp,omitempty"`
	Owner             string    `json:"owner"`
	AccessCount       uint64    `json:"accessCount"`
	MatomoId          string    `json:"matomoId"`
}

type updatePayload struct {
	TargetUrl string `json:"targetUrl"`
	// Description is a pointer to allow the update of the field with an empty string.
	Description *string `json:"description"`
	Owner       string  `json:"owner"`
	// MatomoId is a pointer to allow the update of the field with an empty string.
	MatomoId *string `json:"matomoId"`
}

type errorPayload struct {
	Status   string   `json:"status"`
	Messages []string `json:"messages"`
}

func payloadFromRedirectEntry(e models.RedirectEntry, canonicalHost string) entryPayload {
	p := entryPayload{
		Id:                e.Id,
		Slug:              e.Slug,
		TargetUrl:         e.TargetUrl,
		AppendQuery:       e.AppendQuery,
		Description:       e.Description,
		CreationTimestamp: e.CreationTimestamp,
		DeletionTimestamp: e.DeletionTimestamp,
		Owner:             e.OwnerId,
		AccessCount:       e.AccessCount,
		MatomoId:          e.MatomoId,
	}
	p.Url = fmt.Sprintf("https://%s/%s", canonicalHost, p.Slug)
	return p
}

func payloadFromRedirectEntries(e []models.RedirectEntry, canonicalHost string) []entryPayload {
	entries := []entryPayload{}
	for _, entry := range e {
		entries = append(entries, payloadFromRedirectEntry(entry, canonicalHost))
	}
	return entries
}

type slugPayload struct {
	Slug string `json:"slug"`
}

func payloadFromSlug(slug string) slugPayload {
	p := slugPayload{
		Slug: slug,
	}
	return p
}

type auditPayload struct {
	Action    string    `json:"action"`
	Data      string    `json:"data"`
	RequestId string    `json:"request_id,omitempty"`
	SourceIP  string    `json:"source_ip,omitempty"`
	Timestamp time.Time `json:"timestamp"`
	User      string    `json:"user"`
}

func payloadFromAudits(records []models.AuditRecord) []auditPayload {
	// this ensures that Go's JSON serializer sends an empty list as "[]" instead of "null"
	var payload = make([]auditPayload, 0)

	for _, r := range records {
		payload = append(payload, auditPayload{
			Action:    r.Action,
			RequestId: r.RequestId,
			SourceIP:  r.SourceIP,
			Timestamp: r.Timestamp,
			User:      r.User,
			Data:      string(r.Data),
		})
	}

	return payload
}
