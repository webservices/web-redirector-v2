package cache

// This package acts as a persistent cache between the application instances (pods) and the database (Postgres on DBOD)
// We are doing this not necessiraly for performance reasons, but for availability:
// since this application is serving "cern.ch" and "www.cern.ch", we don't want to to be offline just because there is
// an issue with the database (network unavailability, maintenance, crash etc.)
// Instead, we keep a a simple key-value representation of the currenctly active entries in each application instance (pod).

import (
	"bytes"
	"context"
	"encoding/gob"
	"fmt"
	"time"

	"webredirector/db"
	"webredirector/log"
	"webredirector/models"
	"webredirector/util"

	bolt "go.etcd.io/bbolt"
)

type Bbolt struct {
	db            *bolt.DB
	sourceDb      db.DB
	log           log.Logger
	updateChannel chan string
}

// "Buckets" are bbolt's name for key-value tables
var bucketName = []byte("redirections")

// Creates a new instance of bbolt database (clearing the previous data)
// and initializes the cache from the given source database.
// context can be used to abort intialization or to quit background synchronization task
func NewBbolt(path string, sourceDb db.DB, logger log.Logger, ctx context.Context) (*Bbolt, error) {
	var err error
	bb := new(Bbolt)
	bb.log = logger
	bb.sourceDb = sourceDb

	// set up new bolt db
	bb.log.Debug("Opening bbolt database file '%s'", path)
	bb.db, err = bolt.Open(path, 0600, &bolt.Options{
		Timeout: 1 * time.Second,
	})
	if err != nil {
		return bb, err
	}

	// create initial bucket - make sure that's empty so we don't have any leftover data
	// (the serialization format of the old data might be incompatible with the new one)
	bb.log.Debug("Initializing bucket %s", bucketName)
	err = bb.db.Update(func(tx *bolt.Tx) error {
		err := tx.DeleteBucket(bucketName)
		if err != nil && err != bolt.ErrBucketNotFound {
			return err
		}
		_, err = tx.CreateBucket(bucketName)
		return err
	})
	if err != nil {
		return bb, fmt.Errorf("Failed to initialize bucket %s: %s", bucketName, err)
	}

	// set up update notifications
	bb.log.Debug("Setting up update notifications from source database")
	bb.updateChannel = make(chan string)
	notificationsCtx, cancelNotifications := context.WithCancel(ctx)
	sourceDb.PublishUpdatesTo(notificationsCtx, bb.updateChannel)

	// start listening in the background (non-blocking) for updates
	// this go routine will run until the updateChannel gets closed
	go func() {
		for updateId := range bb.updateChannel {
			bb.log.Debug("Received update notification for '%s'", updateId)
			entries, err := bb.sourceDb.SearchRedirects(ctx, db.SearchOpts{Id: updateId})
			if err == db.ErrNotFound || len(entries) == 0 {
				bb.log.Debug("Deleting entry %s from local cache", updateId)
				bb.deleteEntries([]string{updateId})
				continue
			}
			if err != nil {
				bb.log.Error("Failed to fetch entry %s from source DB: %s", updateId, err)
				continue
			}
			bb.log.Debug("Updating entry %s in local cache", entries[0].Id)
			bb.updateEntries([]models.RedirectEntry{entries[0]})
		}
		bb.log.Info("Update channel got closed, stopping listening for update notifications.")
		cancelNotifications()
	}()

	// populate our cache with active entries from database
	bb.log.Info("Populating cache with active entries")
	startTime := time.Now()
	entries, err := sourceDb.SearchRedirects(ctx, db.SearchOpts{})
	if err != nil {
		return bb, fmt.Errorf("Failed to fetch active redirect ids: %s", err)
	}

	// bbolt does not deal well with very large transactions, therefore we split up the insertion of
	// ALL entries (~100k in prod) into smaller chunks:
	// > Bulk loading a lot of random writes into a new bucket can be slow as the page will not split until the transaction is committed.
	// > Randomly inserting more than 100,000 key/value pairs into a single new bucket in a single transaction is not advised.
	// https://github.com/etcd-io/bbolt
	chunkSize := 10000
	for i := 0; i < len(entries); i += chunkSize {
		start := i
		end := i + chunkSize
		if end > len(entries) {
			end = len(entries)
		}
		chunk := entries[start:end]

		err := bb.updateEntries(chunk)
		if err != nil {
			return bb, fmt.Errorf("Failed to update entries in local cache due to: %s", err)
		}
		bb.log.Info("Loaded %d entries (%d%%)", end, end*100/len(entries))
	}

	bb.log.Info("Successfully initialized bbolt cache with %d entries in %s", len(entries), time.Now().Sub(startTime))
	return bb, nil
}

func (bb *Bbolt) GetRedirectBySlug(ctx context.Context, slug string) (models.RedirectEntry, error) {
	lookupHash := generateLookupHash(slug)
	var entry models.RedirectEntry
	err := bb.db.View(func(tx *bolt.Tx) error {
		buf := tx.Bucket(bucketName).Get(lookupHash)
		if buf == nil || len(buf) == 0 {
			// not found
			return nil
		}

		// decode the byte slice `buf` into an actual Go object `entry`
		// https://godocs.io/encoding/gob#NewDecoder
		return gob.NewDecoder(bytes.NewReader(buf)).Decode(&entry)
	})
	if err != nil {
		// we encountered an error searching in the database, return the error
		return entry, err
	}

	// we found it!
	return entry, nil
}

func (bb *Bbolt) updateEntries(entries []models.RedirectEntry) error {
	return bb.db.Update(func(tx *bolt.Tx) error {
		for _, entry := range entries {
			lookupHash := generateLookupHash(entry.Slug)
			// serialize the object into a byte slice
			// https://go.dev/blog/gob
			var buf bytes.Buffer
			err := gob.NewEncoder(&buf).Encode(entry)
			if err != nil {
				return err
			}

			err = tx.Bucket(bucketName).Put(lookupHash, buf.Bytes())
			if err != nil {
				return err
			}
		}
		return nil
	})
}

func (bb *Bbolt) deleteEntries(ids []string) error {
	return bb.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucketName)
		// since we can't derive the lookup hash of the entry without the slug,
		// we need to loop over all entries in the cache.
		// this approach is certainly not optimal,
		// but since this is a local file that is likely almost entirely in memory,
		// it should still be reasonably fast.
		return b.ForEach(func(k, v []byte) error {
			var entry models.RedirectEntry
			err := gob.NewDecoder(bytes.NewReader(v)).Decode(&entry)
			if err != nil {
				return fmt.Errorf("Unable to decode database entry %s: %s", k, err)
			}
			if util.StringInSlice(entry.Id, ids) {
				// we found an entry that should be deleted
				err = b.Delete(k)
				if err != nil {
					return fmt.Errorf("Unable to delete database entry %s: %s", k, err)
				}
			}
			// all good
			return nil
		})
	})
}

func generateLookupHash(slug string) []byte {
	// for now, this is trivial, but it might change in the future
	return []byte(slug)
}

func (bb *Bbolt) Close() error {
	close(bb.updateChannel)
	return bb.db.Close()
}
