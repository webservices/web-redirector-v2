package cache

import (
	"context"
	"os"
	"testing"
	"time"

	"webredirector/db"
	jsondb "webredirector/db/json"
	"webredirector/db/postgres"
	"webredirector/log"
	"webredirector/models"
)

var syncDelay = 50 * time.Millisecond

func runTestGetRedirect(t *testing.T, sourceDb db.DB) {
	ctx := context.Background()

	// insert an entry (simulates a pre-populated database)
	oldEntry := models.RedirectEntry{
		Slug:      "CherryTomato" + t.Name(),
		TargetUrl: "https://home.web.cern.ch",
		OwnerId:   "FoobarOld",
	}
	err := sourceDb.StoreRedirect(ctx, &oldEntry)
	if err != nil {
		t.Fatalf("Got error while inserting entry: %s", err)
	}

	// set up bbolt cache
	logger := log.NewLogger(log.LogLevelFromString("DEBUG"))
	cache, err := NewBbolt(t.TempDir()+"bbolt.db", sourceDb, logger, ctx)
	if err != nil {
		t.Fatalf("Got error while creating cache DB: %s", err)
	}

	time.Sleep(syncDelay)

	// the existing entry should be present in the cache
	got, err := cache.GetRedirectBySlug(ctx, oldEntry.Slug)
	if err != nil {
		t.Fatalf("Failed to fetch entry %s from cache DB: %s", oldEntry.Slug, err)
	}
	if oldEntry.TargetUrl != got.TargetUrl {
		t.Fatalf("Unexpected target URL, got: %s, wanted: %s", got.TargetUrl, oldEntry.TargetUrl)
	}
	if oldEntry.Id != got.Id {
		t.Fatalf("Unexpected ID, got: %s, wanted: %s", got.Id, oldEntry.Id)
	}

	// create a new entry
	newEntry := models.RedirectEntry{
		Slug:      "Foobar" + t.Name(),
		TargetUrl: "https://cern.ch",
		OwnerId:   "FoobarNew",
	}
	err = sourceDb.StoreRedirect(ctx, &newEntry)
	if err != nil {
		t.Fatalf("Got error while inserting entry: %s", err)
	}

	time.Sleep(syncDelay)

	// the entry should now be present in the cache
	got, err = cache.GetRedirectBySlug(ctx, newEntry.Slug)
	if err != nil {
		t.Fatalf("Failed to fetch entry %s from cache DB: %s", newEntry.Slug, err)
	}
	if newEntry.TargetUrl != got.TargetUrl {
		t.Fatalf("Unexpected target URL, got: %s, wanted: %s", got.TargetUrl, newEntry.TargetUrl)
	}
	if newEntry.Id != got.Id {
		t.Fatalf("Unexpected ID, got: %s, wanted: %s", got.Id, newEntry.Id)
	}

	// delete an entry
	oldEntry.Delete()
	err = sourceDb.StoreRedirect(ctx, &oldEntry)
	if err != nil {
		t.Fatalf("Got error while inserting entry: %s", err)
	}

	time.Sleep(syncDelay)

	// make sure it's no longer present in local cache
	// (i.e. look up a non-existing entry)
	got, err = cache.GetRedirectBySlug(ctx, oldEntry.Slug)
	if err != nil {
		t.Fatalf("Expected no error for non-existing entry, got: %s", err)
	}
	if got != (models.RedirectEntry{}) {
		t.Fatalf("Expected non-existing entry to be not found, got: %v", got)
	}

	// close the cache before closing the source DB
	// this should properly clean up all background tasks
	err = cache.Close()
	if err != nil {
		t.Fatalf("Cache returned error while closing: %s", err)
	}

	err = sourceDb.Close()
	if err != nil {
		t.Fatalf("Database returned error while closing: %s", err)
	}
}

func TestGetRedirectBySlugWithJSONDB(t *testing.T) {
	// set up the source db
	sourceDb, err := jsondb.New("json://" + t.TempDir() + "db.json")
	if err != nil {
		t.Fatalf("Got error while creating source DB: %s", err)
	}

	runTestGetRedirect(t, sourceDb)
}

func TestGetRedirectBySlugWithPostgresDB(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping test in unit-test mode.")
	}

	logger := log.NewLogger(log.LogLevelFromString("DEBUG"))
	// Create a new database
	sourceDb, err := postgres.New(os.Getenv("DB_CONNECTION"), logger)
	if err != nil {
		t.Fatalf("Got error while creating DB: %s", err)
	}
	runTestGetRedirect(t, sourceDb)
}
