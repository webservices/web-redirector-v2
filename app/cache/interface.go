package cache

import (
	"context"
	"webredirector/models"
)

type Cache interface {
	GetRedirectBySlug(context.Context, string) (models.RedirectEntry, error)
	Close() error
}
