package cache

import (
	"context"
	"testing"

	jsondb "webredirector/db/json"
	"webredirector/models"
)

// this is mianly here for test coverage purposes
func TestPassthroughCache(t *testing.T) {
	ctx := context.Background()

	// set up the source db
	sourceDb, err := jsondb.New("json://" + t.TempDir() + "db.json")
	if err != nil {
		t.Fatalf("Got error while creating source DB: %s", err)
	}

	// insert an entry (simulates a pre-populated database)
	oldEntry := models.RedirectEntry{
		Slug:      "CherryTomato" + t.Name(),
		TargetUrl: "https://home.web.cern.ch",
		OwnerId:   "FoobarOld",
	}
	err = sourceDb.StoreRedirect(ctx, &oldEntry)
	if err != nil {
		t.Fatalf("Got error while inserting entry: %s", err)
	}

	// set up fake cache
	cache := NewPassthrough(sourceDb)

	// the existing entry should be present in the cache
	got, err := cache.GetRedirectBySlug(ctx, oldEntry.Slug)
	if err != nil {
		t.Fatalf("Failed to fetch entry %s from cache DB: %s", oldEntry.Slug, err)
	}
	if oldEntry.TargetUrl != got.TargetUrl {
		t.Fatalf("Unexpected target URL, got: %s, wanted: %s", got.TargetUrl, oldEntry.TargetUrl)
	}
	if oldEntry.Id != got.Id {
		t.Fatalf("Unexpected ID, got: %s, wanted: %s", got.Id, oldEntry.Id)
	}

	// delete an entry
	oldEntry.Delete()
	err = sourceDb.StoreRedirect(ctx, &oldEntry)
	if err != nil {
		t.Fatalf("Got error while inserting entry: %s", err)
	}

	// make sure it's no longer present in local cache
	// (i.e. look up a non-existing entry)
	got, err = cache.GetRedirectBySlug(ctx, oldEntry.Slug)
	if err != nil {
		t.Fatalf("Expected no error for non-existing entry, got: %s", err)
	}
	if got != (models.RedirectEntry{}) {
		t.Fatalf("Expected non-existing entry to be not found, got: %v", got)
	}

	err = cache.Close()
	if err != nil {
		t.Fatalf("Cache returned error while closing: %s", err)
	}
}
