package cache

import (
	"context"

	"webredirector/db"
	"webredirector/models"
)

type PassthroughCache struct {
	sourceDb db.DB
}

func NewPassthrough(sourceDb db.DB) Cache {
	c := PassthroughCache{
		sourceDb: sourceDb,
	}
	return &c
}

func (c *PassthroughCache) GetRedirectBySlug(ctx context.Context, slug string) (models.RedirectEntry, error) {
	entries, err := c.sourceDb.SearchRedirects(ctx, db.SearchOpts{Slug: slug})
	if err != nil {
		return models.RedirectEntry{}, err
	}
	if len(entries) < 1 {
		return models.RedirectEntry{}, nil
	}
	if len(entries) > 1 {
		panic("Inconsistent state: SearchRedirects by slug returned more than one entry")
	}
	return entries[0], nil
}

func (c *PassthroughCache) Close() error {
	// nothing to do here
	return nil
}
