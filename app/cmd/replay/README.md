# Replay logs from legacy web redirector

This tool will help us to find minor differences between the old (Windows-based) web-redirector and the new (Go-based) web-redirector V2.

For this purpose, we obtain the logs from the old Windows server for the last year.
The files are named like `u_exXXXXX.log` and contain comments - in the next step, we combine all files into one and strip the comments:

```
grep -a -h -v -E '^#' u_*.log > merged.log
```

A ZIP archive containing the merged logs from 2021-12-31 until 2022-09-13 can be found here (~10GB uncompressed): <https://new.cernbox.cern.ch/files/spaces/eos/project/i/it-pw/it-pw-pi/Web%20Redirector%20V2/Old%20Web%20Redirector%20Logs>

After we have the merged log file, we still want to add the redirection URLs (i.e. the `Location` HTTP header) to the records:

```sh
cd cmd/replay
# hit Ctrl-C to quit the operation
go run . --amend --log-file logs/merged.log --base-url https://cern.ch > logs/amended.log
# Note: errors/warnings are written to STDERR, log lines are written to STDOUT
```

A ZIP archive containing the amended (and filtered) logs (around 30M records) can be found here (~8GB uncompressed): <https://new.cernbox.cern.ch/files/spaces/eos/project/i/it-pw/it-pw-pi/Web%20Redirector%20V2/Old%20Web%20Redirector%20Logs/amended.log.zip>

Finally, we can run the replay tool like this:

```
# run web-redirector-v2 in the background (store logs in `new.log`)
make run-server > new.log 2>&1 &

# run replay tool without request caching
cd cmd/replay
# hit Ctrl-C to quit
go run . --compare --log-file logs/amended.log --base-url http://localhost:3333 --cache=false > diff.log

# alternatively, run it directly against the QA instance (no need to run server in background):
go run . --compare --log-file logs/amended.log --base-url https://web-redirector-v2-qa.web.cern.ch
```

Since it's very likely that we encountered the same difference multiple times, we can filter the output by unqiue entries:

```
sort -u diff.log > unique.log
```
