package main

import (
	"net/http"
	"sync"
)

type requestCache struct {
	requests map[string]*http.Response
	mu       sync.RWMutex
}

func newCache() *requestCache {
	c := requestCache{}
	c.requests = map[string]*http.Response{}
	return &c
}

func (c *requestCache) get(method, url string) *http.Response {
	// take a shared (read) lock on the map while we retrieve the value
	c.mu.RLock()
	defer c.mu.RUnlock()
	resp, ok := c.requests[cacheKey(method, url)]
	if !ok {
		return nil
	}

	// make a copy of the response - the pointer is only valid while we have the shared lock!
	return copyResponse(resp)
}

func (c *requestCache) set(method, url string, resp *http.Response) {
	// take an exclusive write lock on the map while we update it
	c.mu.Lock()
	defer c.mu.Unlock()

	// make a copy of the input response
	resp = copyResponse(resp)

	// store it in the map
	c.requests[cacheKey(method, url)] = resp

	return
}

func copyResponse(orig *http.Response) *http.Response {
	// create a new response object
	resp := http.Response{
		// we copy only the relevant fields for our use-case
		// see full definition in https://godocs.io/net/http#Response
		Status:           orig.Status,
		StatusCode:       orig.StatusCode,
		Proto:            orig.Proto,
		ProtoMajor:       orig.ProtoMajor,
		ProtoMinor:       orig.ProtoMinor,
		Header:           orig.Header,
		ContentLength:    orig.ContentLength,
		TransferEncoding: orig.TransferEncoding,
	}
	return &resp
}

func cacheKey(method, url string) string {
	return method + "+" + url
}
