package main

import (
	"context"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"
	"webredirector/util"
)

type record struct {
	timestamp time.Time
	clientIp  string
	method    string
	uri       string
	userAgent string
	referer   string
	status    int
	location  string
}

func main() {
	// parse arguments
	logfile := flag.String("log-file", "", "Path to a file containing IIS logs")
	baseUrl := flag.String("base-url", "", "URL of the web-redirector instance")
	amend := flag.Bool("amend", false, "")
	compare := flag.Bool("compare", false, "")
	cache := flag.Bool("cache", true, "")

	flag.Parse()

	// set up a signal handler for SIGTERM (Ctrl-C) that will cancel our context
	ctx := util.ContextWithSigterm(context.Background())

	// set up a shared HTTP client to take advantage of connection re-use and keep-alive
	tr := &http.Transport{
		MaxIdleConns:          10,
		IdleConnTimeout:       15 * time.Second,
		ResponseHeaderTimeout: 15 * time.Second,
		DisableKeepAlives:     false,
	}
	httpClient := &http.Client{
		Transport: tr,
		// do not follow redirects
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	var requestCache *requestCache = nil
	if *cache == true {
		requestCache = newCache()
	}

	// run the appropriate action
	var err error
	if *amend == true {
		err = amendLogs(ctx, httpClient, requestCache, *logfile, *baseUrl)
	} else if *compare == true {
		err = compareResponses(ctx, httpClient, requestCache, *logfile, *baseUrl)
	} else {
		flag.Usage()
		log.Fatalln("No action specified. Need to choose -amend or -compare")
	}

	if err != nil {
		log.Fatalln(err)
	}
	log.Println("OK")
}

func compareResponses(ctx context.Context, httpClient *http.Client, requestCache *requestCache, logfile string, newBaseUrl string) error {
	// parse CSV-like structure from given log file
	file, err := os.Open(logfile)
	if err != nil {
		return err
	}

	// https://pkg.go.dev/encoding/csv#example-Reader
	reader := csv.NewReader(file)
	reader.Comma = ' ' // logs are using whitespace as a field separator
	reader.Comment = '#'
	reader.ReuseRecord = true
	reader.LazyQuotes = true

	// loop over all lines
	var nOk, nTotal uint64
	for {
		// if the context gets cancelled, stop processing
		err = ctx.Err()
		if err != nil {
			break
		}
		// read next CSV line and check for errors
		fields, err := reader.Read()
		if err == io.EOF {
			// we are done here
			break
		}
		nTotal += 1
		if err != nil {
			log.Printf("Ignoring malformed line due to: %s", err)
			continue
		}
		record, err := parseFields(fields)
		if err != nil {
			log.Printf("Ignoring malformed line due to: %s", err)
			continue
		}

		err = runRequestAndCompare(requestCache, httpClient, record, newBaseUrl)
		if err != nil {
			fmt.Println(err)
			continue
		}

		nOk += 1
	}

	log.Printf("Successfully processed %d records, failed %d", nOk, nTotal-nOk)

	// return the last error we encountered
	return err
}

func amendLogs(ctx context.Context, httpClient *http.Client, requestCache *requestCache, logfile string, oldBaseUrl string) error {
	// parse CSV-like structure from given log file
	file, err := os.Open(logfile)
	if err != nil {
		return err
	}

	// https://pkg.go.dev/encoding/csv#example-Reader
	reader := csv.NewReader(file)
	reader.Comma = ' ' // logs are using whitespace as a field separator
	reader.Comment = '#'
	reader.ReuseRecord = true
	reader.LazyQuotes = true

	writer := csv.NewWriter(os.Stdout)
	writer.Comma = ' ' // logs are using whitespace as a field separator

	// loop over all lines
	var nOk, nTotal uint64
	for {
		// if the context gets cancelled, stop processing
		err = ctx.Err()
		if err != nil {
			break
		}
		// read next CSV line and check for errors
		fields, err := reader.Read()
		if err == io.EOF {
			// we are done here
			break
		}
		nTotal += 1
		if err != nil {
			log.Printf("Ignoring malformed line due to: %s", err)
			continue
		}
		record, err := parseFields(fields)
		if err != nil {
			log.Printf("Ignoring malformed line due to: %s", err)
			continue
		}

		// make request to old WR to figure out the redirection URL
		var location string = "-" // empty value
		// only run requests where we can actually expect a "Location" header
		if record.status >= 300 && record.status < 400 {
			url := oldBaseUrl + record.uri

			// the old WR does a double redirect for "go" routes, but we want to know the final value:
			// cern.ch/go/xyz -> go.web.cern.ch/go/xyz -> targeturl.example.com
			if strings.HasPrefix(record.uri, "/go/") {
				url = "https://go.web.cern.ch" + record.uri
			}

			// obtain redirection target
			location, err = getLocationForRequest(requestCache, httpClient, record.method, url)
			if err != nil {
				log.Printf("%s", err)
				continue
			}
		}

		if record.status == 302 && location == "" {
			// we were unable to determine a Location header for this URL
			// this means that the entry has been deleted in the meantime (since we are looking at old logs)
			log.Printf("Discarding record '%s' because it does not exist anymore", record.uri)
			continue
		}

		// amend the entry with the location field (if any)
		fields = append(fields, location)

		// write amended entry to stdout
		err = writer.Write(fields)
		if err != nil {
			log.Fatalln("error writing record to csv:", err)
			break
		}

		nOk += 1
	}

	// ensure buffered data is written out
	writer.Flush()

	log.Printf("Successfully processed %d records, failed %d", nOk, nTotal-nOk)

	// return the last error we encountered
	return err
}

func getLocationForRequest(cache *requestCache, httpClient *http.Client, method, url string) (string, error) {
	var resp *http.Response
	// if we have a cache, use that to do the lookup
	if cache != nil && cache.get(method, url) != nil {
		resp = cache.get(method, url)
	} else {
		// run request against old WR and cache the response (status code, location)
		req, err := http.NewRequest(method, url, nil)
		if err != nil {
			return "", fmt.Errorf("%s %s got error: %s", method, url, err)
		}
		resp, err = httpClient.Do(req)
		if err != nil {
			return "", fmt.Errorf("%s %s got error: %s", method, url, err)
		}
		// ensure that we close the request so the httpClient can re-use the connection
		io.Copy(ioutil.Discard, resp.Body)
		resp.Body.Close()

		if cache != nil {
			cache.set(method, url, resp)
		}
	}
	// return location header
	return resp.Header.Get("Location"), nil
}

func runRequestAndCompare(cache *requestCache, httpClient *http.Client, record record, baseUrl string) error {
	if record.method == "PROPFIND" {
		// ignore WebDAV requests, they are invalid on this site
		return nil
	}
	if record.status == 500 {
		// if the old WR returned an internal server error, we cannot know if this was a bug or some other error
		return nil
	}
	if record.uri == "/" {
		// there are lots of requests for "cern.ch/" and "www.cern.ch/" in the logs, but we don't need to repeat all of them
		return nil
	}
	if strings.HasPrefix(record.uri, "/sw/") {
		// this entry has been deleted in the meantime, ignore it
		return nil
	}
	if strings.HasPrefix(record.uri, "/xrootd-ceph-repo/") {
		// this entry has been deleted in the meantime, ignore it
		return nil
	}

	// run request against new WR
	URL := baseUrl + record.uri
	var resp *http.Response
	if cache != nil && cache.get(record.method, URL) != nil {
		resp = cache.get(record.method, URL)
	} else {
		req, err := http.NewRequest(record.method, URL, nil)
		if err != nil {
			return fmt.Errorf("%s %s got error: %s", record.method, URL, err)
		}
		resp, err = httpClient.Do(req)
		if err != nil {
			if strings.Contains(err.Error(), "connection refused") {
				panic("unable to connect to server: " + err.Error())
			}
			return fmt.Errorf("%s %s got error: %s", record.method, URL, err)
		}
		// ensure that we close the request so the httpClient can re-use the connection
		io.Copy(ioutil.Discard, resp.Body)
		resp.Body.Close()

		if cache != nil {
			cache.set(record.method, URL, resp)
		}
	}

	// compare the returned status codes and locations
	oldStatus := record.status
	newStatus := resp.StatusCode
	if newStatus == 405 && oldStatus == 404 {
		// the new web redirector returns 405 (Method not allowed) for some request types, the old one just replied with 404
		return nil
	}
	if newStatus != oldStatus {
		return fmt.Errorf("Old WR returned status code %d, new one returned %d: %s %s", oldStatus, newStatus, record.method, record.uri)
	}

	// compare the returned location header
	oldLocation := record.location
	newLocation := resp.Header.Get("Location")
	if oldLocation == "https://home.web.cern.ch/" && newLocation == "https://home.cern" {
		// the new WR redirects to the canonical URL
		return nil
	}
	if oldLocation != newLocation {
		// the old web-redirector has the non-standard compliant behavior of returning non-encoded URLs in the Location header
		// i.e. when requesting `/flair/download/ubuntu/*18.04`, the old wr returns `https://flair.web.cern.ch/flair/download/ubuntu/*18.04`, when it should return `https://flair.web.cern.ch/flair/download/ubuntu/%2A18.04`
		// see https://www.rfc-editor.org/rfc/rfc3986.html#section-2.2
		decodedLocation, _ := url.QueryUnescape(newLocation)
		if oldLocation != decodedLocation {
			return fmt.Errorf("%s: Old WR returned location '%s', new one returned '%s'", record.uri, oldLocation, newLocation)
		}
	}

	// this entry passed all our checks
	return nil
}

func parseFields(fields []string) (record, error) {
	var r record
	// Fields
	// date time s-ip cs-method cs-uri-stem cs-uri-query s-port cs-username c-ip cs(User-Agent) cs(Referer) cs-host sc-status sc-substatus sc-win32-status time-taken location
	//  0    1     2     3           4           5         6        7         8       9            10         11       12         13            14            15         16
	timestamp, err := time.Parse("2006-01-02 15:04:05", fields[0]+" "+fields[1])
	if err != nil {
		return r, fmt.Errorf("Unable to parse timestamp: %s", err)
	} else {
		r.timestamp = timestamp
	}

	if !isEmpty(fields[3]) {
		r.method = fields[3]
	} else {
		return r, fmt.Errorf("Discarding line because it doesn't have a method")
	}
	if !isEmpty(fields[4]) {
		r.uri = fields[4]
	} else {
		return r, fmt.Errorf("Discarding line because it doesn't have an URI")
	}
	if !isEmpty(fields[5]) {
		r.uri += fields[5]
	}
	if !isEmpty(fields[8]) {
		r.clientIp = fields[8]
	}
	if !isEmpty(fields[9]) {
		r.userAgent = strings.ReplaceAll(fields[9], "+", " ")
	}
	if !isEmpty(fields[10]) {
		r.referer = fields[10]
	}
	if !isEmpty(fields[12]) {
		status, err := strconv.Atoi(fields[12])
		if err != nil {
			return r, fmt.Errorf("Failed to parse status '%s': %s", fields[12], err)

		}
		r.status = status
	}

	// this field is only present on the "amended" logs
	if len(fields) > 16 && !isEmpty(fields[16]) {
		r.location = fields[16]
	}

	return r, nil
}

func isEmpty(s string) bool {
	return s == "" || s == "-"
}
