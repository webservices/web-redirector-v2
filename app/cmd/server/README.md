# web-redirector server

This server implements the main HTTP endpoints which handles redirections and API requests.

## Configuration

The server is configured with environment variables:

NAME | DEFAULT | REQUIRED | EXAMPLE(S)
-----|---------|----------|-----------
`DB_CONNECTION` | `` | yes | `postgresql://${USER}:${PASSWORD}@${HOSTNAME}:${PORT}/${DB_NAME}`
`HTTP_LISTEN_ADDR` | `127.0.0.1:3333` | no |
`LOG_LEVEL` | `INFO` | no | `DEBUG`, `INFO`, `WARN`, `ERROR`, `FATAL`
`WR_CANONICAL_HOST` | `localhost:3333` | no | `cern.ch`
`WR_OIDC_ISSUER_URL` | `https://auth.cern.ch/auth/realms/cern/` | no |
`WR_OIDC_AUDIENCE` | `` | no | `web-redirector-v2-qa` - if unset, the HTTP headers `X-Forwarded-User` and `X-Forwarded-Roles` will be used instead (unsecure!)
`WR_HTTP_LOG_DESTINATION` | `` | no | `syslog-collector:514`
`MATOMO_SERVER_URL` | `` | no | `https://webanalytics.web.cern.ch/`
