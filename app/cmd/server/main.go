package main

import (
	"context"
	"log/syslog"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"webredirector/api"
	"webredirector/cache"
	"webredirector/db/postgres"
	"webredirector/log"
	"webredirector/models"
	"webredirector/util"

	slogsyslog "github.com/samber/slog-syslog"
	"golang.org/x/exp/slog"
)

func main() {
	// make sure rand package is properly seeded everywhere
	rand.Seed(time.Now().UnixNano())

	// shared logger
	logger := log.NewLogger(log.LogLevelFromString(util.GetEnv("LOG_LEVEL", "", false)))

	// intialize config
	config := api.Config{
		OidcAudience:    util.GetEnv("WR_OIDC_AUDIENCE", "", false),
		OidcIssuerUrl:   util.GetEnv("WR_OIDC_ISSUER_URL", "https://auth.cern.ch/auth/realms/cern/", false),
		CanonicalHost:   util.GetEnv("WR_CANONICAL_HOST", "localhost:3333", false),
		MatomoServerUrl: util.GetEnv("MATOMO_SERVER_URL", "https://webanalytics.web.cern.ch", false),
		MatomoAuthToken: util.GetEnv("MATOMO_AUTH_TOKEN", "", false),
	}
	// set up database connection
	connectionString := util.GetEnv("DB_CONNECTION", "", true)
	logger.Debug("Using connection string '%s'", connectionString)
	postgresDb, err := postgres.New(connectionString, logger)
	if err != nil {
		logger.Fatal("Failed to initialize to DB: %s", err)
	}

	// set up local cache
	cacheFile := util.GetEnv("CACHE_FILE", "/tmp/web-redirector-v2.db-cache.bbolt", false)
	cache, err := cache.NewBbolt(cacheFile, postgresDb, logger, context.Background())
	if err != nil {
		logger.Fatal("Failed to initialize local cache at %s: %s", cacheFile, err)
	}

	reservedSlugsFile := util.GetEnv("RESERVED_SLUGS_FILE", "./hack/reserved_slugs.txt", false)
	reservedSlugs, err := util.ParseReservedSlugs(reservedSlugsFile)
	if err != nil {
		logger.Fatal("Failed to read file with reserved slugs: %s", err)
	}
	models.ReservedSlugs = reservedSlugs
	logger.Info("Loaded %d reserved slugs", len(models.ReservedSlugs))

	var httpLogger *slog.Logger
	if httpLogDest := util.GetEnv("WR_HTTP_LOG_DESTINATION", "", false); httpLogDest != "" {
		writer, err := syslog.Dial("udp", httpLogDest, syslog.LOG_INFO|syslog.LOG_DAEMON, "web-redirector-v2")
		if err != nil {
			logger.Fatal("%v", err)
		}

		httpLogger = slog.New(
			slogsyslog.Option{Level: slog.LevelDebug, Writer: writer}.NewSyslogHandler(),
		).With("log_type", "http_access")
		logger.Info("Using endpoint %s for HTTP logs", httpLogDest)
	}

	// set up http handlers etc.
	app := api.NewApp(nil, postgresDb, logger, config, cache, httpLogger)

	// start serving requests
	addr := util.GetEnv("HTTP_LISTEN_ADDR", "127.0.0.1:3333", false)
	serve(addr, app.GetRootHandler(), logger)
}

// serve sets up an http server (at the given address) which supports graceful termination
// https://github.com/go-chi/chi/blob/master/_examples/graceful/main.go
func serve(address string, handler http.Handler, logger log.Logger) {
	httpServer := &http.Server{Addr: address, Handler: handler}

	// Set up a shared context for the server
	serverCtx, serverStopCtx := context.WithCancel(context.Background())

	// Listen for syscall signals for process to interrupt/quit
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	go func() {
		// block until we receive one of the signals listed above
		<-sig

		// Shutdown signal with grace period of 15 seconds
		shutdownCtx, _ := context.WithTimeout(serverCtx, 15*time.Second)

		go func() {
			<-shutdownCtx.Done()
			if shutdownCtx.Err() == context.DeadlineExceeded {
				logger.Fatal("graceful shutdown timed out.. forcing exit.")
			}
		}()

		// Trigger graceful shutdown of the HTTP server
		err := httpServer.Shutdown(shutdownCtx)
		if err != nil {
			logger.Fatal("%s", err)
		}
		serverStopCtx()
	}()

	// Run the server
	logger.Info("Starting to listen on '%s' for HTTP requests", address)
	err := httpServer.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		logger.Fatal("HTTP server exited with: %s", err)
	}

	// Wait for server context to be stopped
	<-serverCtx.Done()
}
