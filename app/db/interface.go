package db

import (
	"context"
	"fmt"

	"webredirector/models"
)

var ErrNotFound = fmt.Errorf("Database entry not found")

type DB interface {
	// this method fetches a full RedirectEntry from the database
	// based on the provided search opts (see SearchOpts struct)
	SearchRedirects(context.Context, SearchOpts) ([]models.RedirectEntry, error)
	// this method saves a RedirectEntry in the database
	// if the entry is new, the entry's ID will be initialized on the passed object
	StoreRedirect(context.Context, *models.RedirectEntry) error
	// this method updates the hit counter for a given entry based on the ID
	IncrementCounter(context.Context, string) error
	// this method sets up a publish-subscribe mechanism for modification of entries
	// whenever an entry is modified, the ID shall be sent on the provided channel
	PublishUpdatesTo(context.Context, chan string)
	// this method stores an audit record in the database
	StoreAudit(context.Context, *models.AuditRecord) error
	// this method retrieves all audit records for a particular redirection
	GetAudits(context.Context, string) ([]models.AuditRecord, error)
	// this method releases the underlying connection, if supported by the driver
	Close() error
}

type SearchOpts struct {
	Id      string
	Slug    string
	OwnerId string
	Deleted bool
}
