// Package json implements an fake database backend intended for testing
package json

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"os"
	"strings"
	"sync"
	"time"

	"webredirector/db"
	"webredirector/models"
)

// this struct implements the "DB" interface
type JSONDB struct {
	filepath     string
	file         *os.File
	fileMutex    sync.Mutex
	updates      chan string
	auditRecords []models.AuditRecord
}

// New returns a new instance of a JSON database
func New(connection string) (db.DB, error) {
	fields := strings.Split(connection, "://")
	if len(fields) != 2 || fields[0] != "json" {
		return nil, fmt.Errorf("Unsupported connection string '%s'", connection)
	}

	file, err := os.Create(fields[1])
	if err != nil {
		return nil, err
	}
	_, err = file.WriteString("[]")
	if err != nil {
		return nil, err
	}

	db := new(JSONDB)
	db.filepath = fields[1]
	db.file = file
	db.updates = make(chan string)

	return db, nil
}

func (j *JSONDB) readallbytes() ([]byte, error) {
	j.fileMutex.Lock()
	defer j.fileMutex.Unlock()
	j.file.Seek(0, 0)
	data, err := io.ReadAll(j.file)
	return data, err
}

func (j *JSONDB) writeallbytes(data []byte) error {
	j.fileMutex.Lock()
	defer j.fileMutex.Unlock()
	// truncate file
	j.file.Seek(0, 0)
	_, err := j.file.Write(data)
	return err
}

func (j *JSONDB) save(data []models.RedirectEntry) error {
	// serialize to JSON
	buf, err := json.Marshal(&data)
	if err != nil {
		return err
	}
	// write bytes to file
	err = j.writeallbytes(buf)
	if err != nil {
		return err
	}
	return nil
}

func (j *JSONDB) load() ([]models.RedirectEntry, error) {
	var data []models.RedirectEntry
	// parse all entries from file
	raw, err := j.readallbytes()
	if err != nil {
		return data, err
	}
	if err := json.Unmarshal(raw, &data); err != nil {
		return data, err
	}
	return data, nil
}

func (j *JSONDB) SearchRedirects(ctx context.Context, opts db.SearchOpts) ([]models.RedirectEntry, error) {
	data, err := j.load()
	if err != nil {
		return []models.RedirectEntry{}, err
	}

	// filter for the relevant one
	entries := []models.RedirectEntry{}
	for _, entry := range data {
		if testEntry(entry, opts) {
			entries = append(entries, entry)
		}
	}

	if len(entries) == 0 {
		return []models.RedirectEntry{}, nil
	}

	return entries, nil
}

func testEntry(entry models.RedirectEntry, opts db.SearchOpts) bool {
	if !(opts.Id == "" || opts.Id == entry.Id) {
		return false
	}

	if !(opts.OwnerId == "" || opts.OwnerId == entry.OwnerId) {
		return false
	}

	if !(opts.Deleted || entry.DeletionTimestamp == time.Time{}) {
		return false
	}

	if !(opts.Slug == "" || opts.Slug == entry.Slug) {
		return false
	}

	// all conditions met
	return true
}

func (j *JSONDB) StoreRedirect(ctx context.Context, entry *models.RedirectEntry) error {
	data, err := j.load()
	if err != nil {
		return err
	}

	if entry.Id != "" {
		// update an existing entry
		for i, val := range data {
			if val.Id == entry.Id {
				data[i] = *entry
				break
			}
		}
	} else {
		// generate random id
		entry.Id = fmt.Sprintf("%d", rand.Intn(1000000))

		// append new entry
		data = append(data, *entry)
	}

	// update DB again
	err = j.save(data)
	if err != nil {
		return err
	}

	// send update notification asynchronously (without blocking)
	id := entry.Id // copy the id so we can already free the object
	go func() {
		fmt.Println("Sending update for entry", id)
		j.updates <- id
	}()

	return nil
}

func (j *JSONDB) IncrementCounter(ctx context.Context, id string) error {
	// not supported
	return nil
}

func (j *JSONDB) Close() error {
	// nothing to do here
	return nil
}

func (j *JSONDB) PublishUpdatesTo(ctx context.Context, updates chan string) {
	// this go-routine will run until the updates channel gets closed
	go func() {
		// take a value out of our internal channel and send it to the channel supplied by the caller
		for msg := range j.updates {
			updates <- msg
		}
	}()
}

func (j *JSONDB) StoreAudit(ctx context.Context, record *models.AuditRecord) error {
	if record.Timestamp.IsZero() {
		record.Timestamp = time.Now()
	}
	record.AuditId = fmt.Sprint(len(j.auditRecords))
	r := *record // copy input
	j.auditRecords = append(j.auditRecords, r)
	return nil
}

func (j *JSONDB) GetAudits(ctx context.Context, redirectEntryId string) ([]models.AuditRecord, error) {
	var records []models.AuditRecord
	for _, r := range j.auditRecords {
		if r.RedirectEntryId == redirectEntryId {
			records = append(records, r)
		}
	}
	return records, nil
}
