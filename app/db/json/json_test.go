package json

import (
	"context"
	"testing"
	"time"

	"webredirector/db"
	"webredirector/models"
)

func TestJsonDb(t *testing.T) {
	// Create a new database
	jsondb, err := New("json://" + t.TempDir() + "db.json")
	if err != nil {
		t.Fatalf("Got error while creating DB: %s", err)
	}

	e := models.RedirectEntry{
		Slug:        "Foobar",
		TargetUrl:   "https://cern.ch",
		Description: "This is a test",
	}

	ctx := context.Background()

	// Insert entry
	err = jsondb.StoreRedirect(ctx, &e)
	if err != nil {
		t.Fatalf("Got error while inserting entry: %s", err)
	}

	// Retrieve entry
	entries, err := jsondb.SearchRedirects(ctx, db.SearchOpts{Slug: e.Slug})
	if err != nil {
		t.Fatalf("Got error while retrieving entry: %s", err)
	}
	if entries[0].Id == "" {
		t.Fatalf("Entry does not have an ID!")
	}

	// retrieve by id
	entries, err = jsondb.SearchRedirects(ctx, db.SearchOpts{Id: e.Id})
	if err != nil {
		t.Fatalf("Got error while retrieving entry: %s", err)
	}
	if entries[0].Id != e.Id {
		t.Fatalf("Entry does not have expected ID '%s', got: %+v", e.Id, entries[0])
	}

	// Update entry
	e.Description = "Updated description"
	err = jsondb.StoreRedirect(ctx, &e)
	if err != nil {
		t.Fatalf("Got error while updating entry: %s", err)
	}

	// Retrieve it again
	entries, err = jsondb.SearchRedirects(ctx, db.SearchOpts{Slug: e.Slug})
	if err != nil {
		t.Fatalf("Got error while retrieving entry: %s", err)
	}
	if entries[0].Id != e.Id {
		t.Fatalf("Entry does not have expected ID '%s', got: %+v", e.Id, entries[0])
	}
	if e.Description != "Updated description" {
		t.Fatalf("Expected description 'Updated description', got '%s'", e.Description)
	}

	// Test active redirections
	want := []models.RedirectEntry{e}
	got, err := jsondb.SearchRedirects(ctx, db.SearchOpts{})
	if len(want) != len(got) || want[0].Id != got[0].Id {
		t.Fatalf("Unexpected ids returned by SearchRedirects: expected %#v, got %#v", want, got)
	}

	// Try to retrieve a non-existing entry
	entries, err = jsondb.SearchRedirects(ctx, db.SearchOpts{Slug: "not-me"})
	if err != nil {
		t.Fatalf("Expected DB error not found for non-existing entry, got: %s", err)
	}
	if len(entries) != 0 {
		t.Fatalf("Expected 0 entries for non-existing slug, got: %v", entries)
	}
}

func TestPublishUpdates(t *testing.T) {
	// Create a new database
	jsondb, err := New("json://" + t.TempDir() + "db.json")
	if err != nil {
		t.Fatalf("Got error while creating DB: %s", err)
	}

	e := models.RedirectEntry{
		Slug:        "Foobar",
		TargetUrl:   "https://cern.ch",
		Description: "This is a test",
	}

	ctx := context.Background()

	// Insert entry
	err = jsondb.StoreRedirect(ctx, &e)
	if err != nil {
		t.Fatalf("Got error while inserting entry: %s", err)
	}

	// check that the pub-sub mechanism worked
	updateChannel := make(chan string)
	receivedMessage := ""
	// close the channel after 5 seconds
	go func() {
		time.Sleep(5 * time.Second)
		close(updateChannel)
	}()
	jsondb.PublishUpdatesTo(ctx, updateChannel)
	for {
		// read from the channel into the variable
		receivedMessage = <-updateChannel
		break
	}

	if receivedMessage == "" {
		t.Fatal("PubSub failed, no message received after 5 seconds")
	}
	if receivedMessage != e.Id {
		t.Fatalf("PubSub failed, expected message '%s', got '%s'", e.Id, receivedMessage)
	}
}
