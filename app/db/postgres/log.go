package postgres

import (
	"context"
	"fmt"
	"webredirector/log"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/tracelog"
)

// this type implements the pgx.Logger interface
type PgxLogger struct {
	logger log.Logger
}

func (l *PgxLogger) Log(ctx context.Context, level tracelog.LogLevel, msg string, data map[string]any) {
	for k, v := range data {
		msg = fmt.Sprintf("%s, %s: %v", msg, k, v)
	}
	switch level {
	case tracelog.LogLevelTrace:
		l.logger.Debug("%s", msg)
	case tracelog.LogLevelDebug:
		l.logger.Debug("%s", msg)
	case tracelog.LogLevelInfo:
		// pgx sends query traces on log level "info", but we only want to see those in debug mode
		l.logger.Debug("%s", msg)
	case tracelog.LogLevelWarn:
		l.logger.Warn("%s", msg)
	case tracelog.LogLevelError:
		l.logger.Error("%s", msg)
	case tracelog.LogLevelNone:
		l.logger.Error("%s", msg)
	default:
		l.logger.Error("%s", msg)
	}
}

// wraps our logging implementation into the expected interface
// https://godocs.io/github.com/jackc/pgx/v5/tracelog
// https://godocs.io/github.com/jackc/pgx/v5#QueryTracer
func getTracerFromLogger(logger log.Logger) pgx.QueryTracer {
	pgxLogger := PgxLogger{logger: logger}
	pgxTracer := tracelog.TraceLog{
		Logger:   &pgxLogger,
		LogLevel: tracelog.LogLevelTrace,
	}

	return &pgxTracer
}
