-- create schema
-- load extension for generating UUIDs
CREATE EXTENSION IF NOT EXISTS "pgcrypto";

-- create table
CREATE TABLE IF NOT EXISTS redirections (
       id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
       slug TEXT NOT NULL UNIQUE,
       target_url TEXT NOT NULL,
       creation_timestamp TIMESTAMP DEFAULT NOW(),
       deletion_timestamp TIMESTAMP,
       owner_id TEXT NOT NULL,
       description TEXT,
       append_query BOOLEAN NOT NULL
       );

-- allows us to efficiently look up namespace-key pairs (common redirection use-case)
CREATE INDEX IF NOT EXISTS redirection_slug_idx ON redirections(slug);
-- alows us to efficiently look up redirects by owner (for API)
CREATE INDEX IF NOT EXISTS redirection_owner_idx ON redirections(owner_id);

---- create above / drop below ----

DROP TABLE IF EXISTS redirections;
-- Note: Postgres automatically deletes the associated indexes and constraints
