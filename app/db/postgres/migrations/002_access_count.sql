CREATE TABLE IF NOT EXISTS access_count (
        redirection_id uuid NOT NULL,
        count int NOT NULL,
        last_access TIMESTAMP DEFAULT NOW(),
        PRIMARY KEY (redirection_id),
        CONSTRAINT fk_redirection_id FOREIGN KEY (redirection_id) REFERENCES redirections (id)
);

---- create above / drop below ----

DROP TABLE IF EXISTS access_count;
-- Note: Postgres automatically deletes the associated indexes and constraints
