ALTER TABLE redirections
      DROP CONSTRAINT redirections_slug_key,
      ADD CONSTRAINT redirections_slug_deletion_timestamp_key UNIQUE(slug, deletion_timestamp);

---- create above / drop below ----

ALTER TABLE redirections
      DROP CONSTRAINT redirections_slug_deletion_timestamp_key,
      ADD CONSTRAINT redirections_slug_key UNIQUE(slug);
