ALTER TABLE access_count
  DROP CONSTRAINT fk_redirection_id,
  ADD CONSTRAINT fk_redirection_id_cascade FOREIGN KEY (redirection_id) REFERENCES redirections(id) ON DELETE CASCADE;

---- create above / drop below ----
