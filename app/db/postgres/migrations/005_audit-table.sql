CREATE TABLE IF NOT EXISTS audit_records (
       action TEXT NOT NULL,
       request_id TEXT,
       source_ip TEXT,
       user_id TEXT,
       audit_id SERIAL,
       redirection_id uuid NOT NULL,
       timestamp TIMESTAMP DEFAULT NOW(),
       data JSONB NOT NULL,
       PRIMARY KEY (audit_id),
       CONSTRAINT fk_redirection_id FOREIGN KEY (redirection_id) REFERENCES redirections (id)
);

-- allows us to efficiently look up audit records for a given redirection id
CREATE INDEX IF NOT EXISTS audit_records_redirection_id_idx ON audit_records(redirection_id);

---- create above / drop below ----

DROP TABLE IF EXISTS audit;
-- Note: Postgres automatically deletes the associated indexes and constraints
