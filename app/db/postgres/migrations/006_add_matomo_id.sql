ALTER TABLE redirections
      ADD matomo_id TEXT DEFAULT '';

---- create above / drop below ----

ALTER TABLE redirections
      DROP COLUMN IF EXISTS matomo_id;
