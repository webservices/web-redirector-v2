package postgres

import (
	"bytes"
	"context"
	"database/sql"
	"fmt"
	"time"

	"webredirector/db"
	"webredirector/log"
	"webredirector/models"

	"github.com/jackc/pgx/v5/pgtype/zeronull"
	"github.com/jackc/pgx/v5/pgxpool"
)

type PostgresDB struct {
	pool *pgxpool.Pool
	log  log.Logger
}

func New(connection string, logger log.Logger) (db.DB, error) {
	pdb := new(PostgresDB)

	// connect to the database
	// connection string example: "postgresql://username:password@localhost:5432/database_name"
	config, err := pgxpool.ParseConfig(connection)
	if err != nil {
		return pdb, err
	}
	config.ConnConfig.Tracer = getTracerFromLogger(logger)

	// use "pgxpool" instead of "pgx" because the raw connection returned by "pgx.Connect()"
	// is not safe for concurrent connections
	// https://github.com/jackc/pgx/wiki/Getting-started-with-pgx
	pool, err := pgxpool.NewWithConfig(context.Background(), config)
	if err != nil {
		return pdb, err
	}

	// make sure we can talk to the DB (executes an empty SQL statement)
	err = pool.Ping(context.Background())
	if err != nil {
		return pdb, err
	}

	// finish initialization of PostgresDB object
	pdb.pool = pool
	pdb.log = logger

	err = pdb.initUpdateNotifications()
	if err != nil {
		return pdb, err
	}

	logger.Info("Successfully connected to database")
	return pdb, nil
}

func (pdb *PostgresDB) Close() error {
	pdb.pool.Close()
	return nil
}

func (pdb *PostgresDB) SearchRedirects(ctx context.Context, opts db.SearchOpts) ([]models.RedirectEntry, error) {
	// use a byte buffer (not a `string`) to efficiently build the query, see:
	// https://www.gobeyond.dev/fmt/#formatting-to-a-string
	var searchQuery bytes.Buffer
	// the base search query
	searchQuery.Write([]byte(`
SELECT id, slug, target_url, creation_timestamp, deletion_timestamp, owner_id, description, append_query, coalesce(count, 0), matomo_id
FROM redirections
LEFT JOIN access_count ON id = redirection_id
WHERE 1 = 1`))

	// Note: *ALWAYS* use prepared queries for adding external input (id, slug etc.),
	// do *NOT* use string concatenation for this purpose!
	// https://owasp.org/www-community/attacks/SQL_Injection

	// append optional queries and arguments based on the given SearchOpts
	searchArgsPtr := new([]interface{})
	searchArgs := *searchArgsPtr
	if opts.Id != "" {
		searchArgs = append(searchArgs, opts.Id)
		fmt.Fprintf(&searchQuery, ` AND id = $%d`, len(searchArgs))
	}
	if opts.Slug != "" {
		searchArgs = append(searchArgs, opts.Slug)
		fmt.Fprintf(&searchQuery, ` AND slug = $%d`, len(searchArgs))
	}
	if opts.OwnerId != "" {
		searchArgs = append(searchArgs, opts.OwnerId)
		fmt.Fprintf(&searchQuery, ` AND owner_id = $%d`, len(searchArgs))
	}
	if opts.Deleted {
		fmt.Fprintf(&searchQuery, ` AND deletion_timestamp <= now()`)
	} else {
		fmt.Fprintf(&searchQuery, ` AND deletion_timestamp IS NULL`)
	}

	fmt.Fprintf(&searchQuery, ` ORDER BY creation_timestamp ASC`)

	entries := []models.RedirectEntry{}
	deletionTimestamp := sql.NullTime{}
	// execute the query on the database
	rows, err := pdb.pool.Query(ctx, searchQuery.String(), searchArgs...)
	if err != nil {
		return entries, err
	}

	// loop over the entries we have received
	for rows.Next() {
		e := models.RedirectEntry{}
		err = rows.Scan(
			&e.Id, &e.Slug, &e.TargetUrl, &e.CreationTimestamp, &deletionTimestamp, &e.OwnerId, &e.Description, &e.AppendQuery, &e.AccessCount, &e.MatomoId,
		)
		if err != nil {
			return entries, err
		}
		// concatenate all entries into a slice
		entries = append(entries, e)
	}

	// Return the entries
	// Note that if we could not find *any* entries, we simply return an empty list, which needs to be handled by the caller.
	return entries, nil
}

func (pdb *PostgresDB) StoreRedirect(ctx context.Context, e *models.RedirectEntry) error {
	if e.Id == "" {
		err := pdb.pool.QueryRow(ctx, `
		INSERT INTO redirections (slug, target_url, creation_timestamp, deletion_timestamp, owner_id, description, append_query, matomo_id)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
		RETURNING id
		`, e.Slug, e.TargetUrl, e.CreationTimestamp, zeronull.Timestamp(e.DeletionTimestamp), e.OwnerId, e.Description, e.AppendQuery, e.MatomoId,
		).Scan(&e.Id)
		if err != nil {
			return err
		}
	} else {
		err := pdb.pool.QueryRow(ctx, `
		UPDATE redirections
		SET target_url = $2, deletion_timestamp = $3, owner_id = $4, description = $5, matomo_id = $6
		WHERE id = $1
		RETURNING id
		`, e.Id, e.TargetUrl, zeronull.Timestamp(e.DeletionTimestamp), e.OwnerId, e.Description, e.MatomoId,
		).Scan(&e.Id)
		if err != nil {
			return err
		}
	}

	return nil
}

func (pdb *PostgresDB) IncrementCounter(ctx context.Context, id string) error {
	err := pdb.pool.QueryRow(ctx, `
		INSERT INTO access_count (redirection_id, count)
		VALUES ($1, 1)
		ON CONFLICT (redirection_id)
		DO
			UPDATE SET count = access_count.count + 1, last_access = NOW()
			WHERE access_count.redirection_id = $1
		RETURNING redirection_id
		`, id,
	).Scan(&id)
	if err != nil {
		return err
	}
	return nil
}

const updateRedirectionsChannel = "redirection_updates"

func (pdb *PostgresDB) initUpdateNotifications() error {
	_, err := pdb.pool.Exec(context.Background(), `
BEGIN;

-- Postgres doesn't support concurrent CREATE FUNCTION statements: https://stackoverflow.com/a/44101303
-- Take an explicit advisory lock to avoid errors like "ERROR: tuple concurrently updated (SQLSTATE XX000)"
-- the integer is used as our lock identifier
SELECT pg_advisory_xact_lock(208666217139);

-- https://tapoueh.org/blog/2018/07/postgresql-listen-notify/
CREATE OR REPLACE FUNCTION notify_on_redirections_update ()
 RETURNS trigger
 LANGUAGE plpgsql
AS $$
DECLARE
  channel TEXT := TG_ARGV[0];
BEGIN
  PERFORM (
     SELECT pg_notify(channel, NEW.id::TEXT)
  );
  RETURN NULL;
END;
$$;

-- https://www.postgresql.org/docs/12/sql-createtrigger.html
DROP TRIGGER IF EXISTS notify_redirections ON redirections;
CREATE TRIGGER notify_redirections
         AFTER INSERT OR UPDATE
            ON redirections
      FOR EACH ROW
       EXECUTE PROCEDURE notify_on_redirections_update(`+updateRedirectionsChannel+`);

COMMIT;
`)
	if err != nil {
		return err
	}
	return nil
}

// This method watches the database for changes and publishes each changed entry's id
// to the given channel.
// It is non-blocking and runs in the background (after initialization).
// To stop the listening, the given context should be cancelled.
// Based on: https://github.com/jackc/pgx/blob/v5-dev/examples/chat/main.go
// and: https://github.com/jackc/pgx/issues/1121#issuecomment-991037026
func (pdb *PostgresDB) PublishUpdatesTo(ctx context.Context, updatedIds chan string) {
	// Keep this task running in the background
	// This go-routine will run until the supplied context gets cancelled (or times out)
	// https://go.dev/doc/database/cancel-operations
	go func() {
		for {
			err := publishUpdatesOnce(ctx, pdb.pool, updatedIds)
			if err != nil {
				pdb.log.Error("publishUpdatesOnce returned: %s", err)
			}

			select {
			case <-ctx.Done():
				return
			default:
				// If publishUpdatesOnce returned and ctx has not been cancelled that means there was a fatal database error.
				// Wait a while to avoid busy-looping while the database is unreachable.
				time.Sleep(time.Minute)
			}
		}
	}()

	// wait for connection setup to finish before returning
	// since it's running in a background goroutine, we don't know exactly when it's done
	time.Sleep(1 * time.Second)
}

func publishUpdatesOnce(ctx context.Context, pgxpool *pgxpool.Pool, updatedIds chan string) error {
	conn, err := pgxpool.Acquire(ctx)
	if err != nil {
		return fmt.Errorf("PublishUpdatesTo failed to acquire database connection: %s", err)
	}
	defer conn.Release()

	_, err = conn.Exec(ctx, "listen "+updateRedirectionsChannel)
	if err != nil {
		return fmt.Errorf("PublishUpdatesTo failed setup listener for channel '%s': %s", updateRedirectionsChannel, err)
	}

	for {
		notification, err := conn.Conn().WaitForNotification(ctx)
		if err != nil {
			if ctx.Err() != nil { // context got cancelled, meaning we should stop listening. this is not an error
				conn.Release()
				return nil
			}
			return fmt.Errorf("PublishUpdatesTo got error waiting for notification: %s", err)
		}

		if notification.Channel == updateRedirectionsChannel {
			updatedIds <- notification.Payload
		}
	}
}

func (pdb *PostgresDB) StoreAudit(ctx context.Context, record *models.AuditRecord) error {
	err := pdb.pool.QueryRow(ctx, `
		INSERT INTO audit_records (action, request_id, source_ip, user_id, redirection_id, data)
		VALUES ($1, $2, $3, $4, $5, $6)
		RETURNING audit_id, timestamp
		`, record.Action, record.RequestId, record.SourceIP, record.User, record.RedirectEntryId, record.Data,
	).Scan(&record.AuditId, &record.Timestamp)
	if err != nil {
		return err
	}
	return nil
}

func (pdb *PostgresDB) GetAudits(ctx context.Context, redirection_id string) ([]models.AuditRecord, error) {
	var records []models.AuditRecord
	// execute the query on the database
	rows, err := pdb.pool.Query(ctx, `
		SELECT action, request_id, source_ip, user_id, audit_id, redirection_id, timestamp, data
        FROM audit_records
        WHERE redirection_id = $1
        ORDER BY timestamp ASC
        `,
		redirection_id)
	if err != nil {
		return records, err
	}

	// loop over the entries we have received
	for rows.Next() {
		r := models.AuditRecord{}
		err = rows.Scan(
			&r.Action, &r.RequestId, &r.SourceIP, &r.User, &r.AuditId, &r.RedirectEntryId, &r.Timestamp, &r.Data,
		)
		if err != nil {
			return records, err
		}
		// concatenate all entries into a slice
		records = append(records, r)
	}

	return records, nil

}
