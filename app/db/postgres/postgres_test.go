package postgres

import (
	"context"
	"fmt"
	"os"
	"strings"
	"testing"
	"time"

	"webredirector/db"
	"webredirector/log"
	"webredirector/models"
)

func TestPostgresDb(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping test in unit-test mode.")
	}

	logger := log.NewLogger(log.LogLevelFromString("DEBUG"))

	// Create a new database
	pg, err := New(os.Getenv("DB_CONNECTION"), logger)
	if err != nil {
		t.Fatalf("Got error while creating DB: %s", err)
	}

	e := models.RedirectEntry{
		Slug:        strings.ToLower(fmt.Sprintf("%s%d", t.Name(), time.Now().UnixMilli())),
		TargetUrl:   "https://cern.ch",
		Description: "This is a test",
	}

	ctx := context.Background()

	// Insert entry
	err = pg.StoreRedirect(ctx, &e)
	if err != nil {
		t.Fatalf("Got error while inserting entry: %s", err)
	}

	// Retrieve entry
	entries, err := pg.SearchRedirects(ctx, db.SearchOpts{Slug: e.Slug})
	if err != nil {
		t.Fatalf("Got error while retrieving entry: %s", err)
	}
	if entries[0].Id == "" {
		t.Fatalf("Entry does not have an ID!")
	}
	if entries[0].Id != e.Id {
		t.Fatalf("Entry does not have expected ID '%s', got: %+v", e.Id, entries[0])
	}

	// retrieve by id
	entries, err = pg.SearchRedirects(ctx, db.SearchOpts{Id: entries[0].Id})
	if err != nil {
		t.Fatalf("Got error while retrieving entry: %s", err)
	}

	// Update entry
	e.Description = "Updated description"
	err = pg.StoreRedirect(ctx, &e)
	if err != nil {
		t.Fatalf("Got error while updating entry: %s", err)
	}

	// Retrieve it again
	entries, err = pg.SearchRedirects(ctx, db.SearchOpts{Slug: e.Slug})
	if err != nil {
		t.Fatalf("Got error while retrieving entry: %s", err)
	}
	if entries[0].Description != "Updated description" {
		t.Fatalf("Expected description 'Updated description', got '%s'", e.Description)
	}

	// Try to retrieve a non-existing entry
	entries, err = pg.SearchRedirects(ctx, db.SearchOpts{Slug: "not-me"})
	if err != nil {
		t.Fatalf("Expected no DB error for non-existing entry, got: %s", err)
	}
	if len(entries) != 0 {
		t.Fatalf("Expected 0 entries for non-existing slug, got: %v", entries)
	}

	// Ensure it's in the returned entries when we dump them all (empty SearchOpts)
	allEntries, err := pg.SearchRedirects(ctx, db.SearchOpts{})
	if err != nil {
		t.Fatalf("SearchRedirects with empty SearchOpts{} failed: %s", err)
	}
	found := false
	for _, entry := range allEntries {
		if entry.Id == e.Id {
			found = true
			break
		}
	}
	if found == false {
		t.Fatalf("Failed to find entry with id '%s' after dumping all entries", e.Id)
	}

	// Test access counter and retrieval by Id
	err = pg.IncrementCounter(ctx, e.Id)
	if err != nil {
		t.Fatalf("Got error while incrementing access count: %s", err)
	}
	entries, err = pg.SearchRedirects(ctx, db.SearchOpts{Id: e.Id})
	if err != nil {
		t.Fatalf("Got error while retrieving entry: %s", err)
	}
	if entries[0].AccessCount != 1 {
		t.Fatalf("Unexpected access count: expected %#v, got %#v", 1, entries[0].AccessCount)
	}

	err = pg.Close()
	if err != nil {
		t.Fatalf("Close returned error: %s", err)
	}
}

func TestPostgresGetByOwner(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping test in unit-test mode.")
	}

	logger := log.NewLogger(log.LogLevelFromString("DEBUG"))

	// Create a new database
	pg, err := New(os.Getenv("DB_CONNECTION"), logger)
	if err != nil {
		t.Fatalf("Got error while creating DB: %s", err)
	}

	owner := "meee" + t.Name()
	e := models.RedirectEntry{
		Slug:        strings.ToLower(fmt.Sprintf("%s%d", t.Name(), time.Now().UnixMilli())),
		TargetUrl:   "https://cern.ch",
		Description: "This is a test",
		OwnerId:     owner,
	}

	ctx := context.Background()
	// Insert entry
	err = pg.StoreRedirect(ctx, &e)
	if err != nil {
		t.Fatalf("Got error while inserting entry: %s", err)
	}

	// Retrieve entry by owner
	entries, err := pg.SearchRedirects(ctx, db.SearchOpts{OwnerId: owner})
	if err != nil {
		t.Fatalf("Got error while retrieving entry: %s", err)
	}
	if len(entries) < 1 {
		t.Fatalf("No entries found for %s", owner)
	}
	foundId := false
	for _, entry := range entries {
		if e.Id == entry.Id {
			foundId = true
			break
		}
	}
	if foundId == false {
		t.Fatalf("Found no entries for owner %s with id %s", owner, e.Id)
	}

	// Search for an invalid owner (should not yield any results)
	entries, err = pg.SearchRedirects(ctx, db.SearchOpts{OwnerId: "Robert'); DROP TABLE students;--"})
	if err != nil && len(entries) >= 1 {
		t.Fatalf("Expected nil error and 0 entries, got: %s %d", err, len(entries))
	}

	err = pg.Close()
	if err != nil {
		t.Fatalf("Close returned error: %s", err)
	}
}

func TestPostgresDeleted(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping test in unit-test mode.")
	}

	logger := log.NewLogger(log.LogLevelFromString("DEBUG"))

	// Create a new database
	pg, err := New(os.Getenv("DB_CONNECTION"), logger)
	if err != nil {
		t.Fatalf("Got error while creating DB: %s", err)
	}

	owner := "meee" + t.Name()
	slug := strings.ToLower(fmt.Sprintf("%s%d", t.Name(), time.Now().UnixMilli()))
	e := models.RedirectEntry{
		Slug:        slug,
		TargetUrl:   "https://cern.ch",
		Description: "This is a test",
		OwnerId:     owner,
	}

	ctx := context.Background()
	// Insert entry
	err = pg.StoreRedirect(ctx, &e)
	if err != nil {
		t.Fatalf("Got error while inserting entry: %s", err)
	}

	// It should be part of the "active" entries
	entries, err := pg.SearchRedirects(ctx, db.SearchOpts{Slug: e.Slug})
	if err != nil {
		t.Fatalf("Got error while retrieving entry: %s", err)
	}
	if len(entries) < 1 {
		t.Fatalf("No entries found for %s", owner)
	}

	// It should NOT be part of the "deleted" entries
	entries, err = pg.SearchRedirects(ctx, db.SearchOpts{Slug: e.Slug, Deleted: true})
	if err != nil {
		t.Fatalf("Got error while retrieving entry: %s", err)
	}
	if len(entries) != 0 {
		t.Fatalf("Expected 0 entries, got %d", len(entries))
	}

	// Delete the entry
	e.Delete()
	pg.StoreRedirect(ctx, &e)
	if err != nil {
		t.Fatalf("Got error while inserting deleted entry: %s", err)
	}

	// It should NOT be part of the "active" entries
	entries, err = pg.SearchRedirects(ctx, db.SearchOpts{Slug: e.Slug})
	if err != nil {
		t.Fatalf("Got error while retrieving entry: %s", err)
	}
	if len(entries) > 0 {
		t.Fatalf("Expected 0 entries, got %d", len(entries))
	}

	// It should be part of the "deleted" entries
	entries, err = pg.SearchRedirects(ctx, db.SearchOpts{Slug: e.Slug, Deleted: true})
	if err != nil {
		t.Fatalf("Got error while retrieving entry: %s", err)
	}
	if len(entries) >= 1 {
		t.Fatalf("Expected at least 1 entry, got %d", len(entries))
	}

	// We should be able to create a new entry with the same slug immediately afterwards
	eNew := models.RedirectEntry{
		Slug:        slug,
		TargetUrl:   "http://example.com",
		Description: "This is test number 2",
		OwnerId:     owner,
	}
	err = pg.StoreRedirect(ctx, &eNew)
	if err != nil {
		t.Fatalf("Got error while inserting entry: %s", err)
	}

	err = pg.Close()
	if err != nil {
		t.Fatalf("Close returned error: %s", err)
	}
}

func TestPublishUpdates(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping test in unit-test mode.")
	}

	logger := log.NewLogger(log.LogLevelFromString("DEBUG"))
	// Create a new database
	pg, err := New(os.Getenv("DB_CONNECTION"), logger)
	if err != nil {
		t.Fatalf("Got error while creating DB: %s", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)

	// set up the pubsub mechanism
	updateChannel := make(chan string)
	// automatically close the channel after some time - this implements our "timeout" for the test
	var timeout = 20 * time.Second
	go func() {
		time.Sleep(timeout)
		_, ok := (<-updateChannel)
		if ok {
			// only close the channel if it is still open
			close(updateChannel)
		}
	}()
	pg.PublishUpdatesTo(ctx, updateChannel)

	// empty the channel in case there were any left-over messages
	receivedMessage := ""
	for {
		// read from the channel into the variable
		receivedMessage = <-updateChannel
		break
	}

	// Insert a new entry
	e := models.RedirectEntry{
		Slug:      strings.ToLower("Foobar" + t.Name()),
		TargetUrl: "https://cern.ch",
		OwnerId:   "Meeee",
	}
	err = pg.StoreRedirect(ctx, &e)
	if err != nil {
		t.Fatalf("Got error while inserting entry: %s", err)
	}

	// check that the pub-sub mechanism worked
	for {
		// read from the channel into the variable
		receivedMessage = <-updateChannel
		break
	}
	if receivedMessage == "" {
		t.Fatalf("PubSub failed, no message received after %s", timeout)
	}
	if receivedMessage != e.Id {
		t.Fatalf("PubSub failed, expected message '%s', got '%s'", e.Id, receivedMessage)
	}

	// cancel listening for updates in the background
	cancel()
	err = pg.Close()
	if err != nil {
		t.Fatalf("Close returned error: %s", err)
	}
}
