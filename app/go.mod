module webredirector

go 1.19

require (
	github.com/MicahParks/keyfunc v1.9.0
	github.com/go-chi/chi/v5 v5.0.7
	github.com/golang-jwt/jwt/v4 v4.5.0
	github.com/jackc/pgx/v5 v5.3.1
	github.com/samber/slog-syslog v0.2.0
	go.etcd.io/bbolt v1.3.7
	golang.org/x/exp v0.0.0-20230425010034-47ecfdc1ba53
)

require (
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/jackc/puddle/v2 v2.2.0 // indirect
	golang.org/x/crypto v0.7.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
	golang.org/x/text v0.8.0 // indirect
)
