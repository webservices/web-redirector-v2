package log

import (
	"log"
	"os"
	"strings"

	"golang.org/x/exp/slog"
)

const (
	LEVEL_FATAL = 0
	LEVEL_ERROR = 1
	LEVEL_WARN  = 2
	LEVEL_INFO  = 3
	LEVEL_DEBUG = 4
)

type Logger interface {
	Debug(string, ...any)
	Info(string, ...any)
	Warn(string, ...any)
	Error(string, ...any)
	Fatal(string, ...any)
}

func NewLogger(level int64) Logger {
	if level < LEVEL_FATAL || level > LEVEL_DEBUG {
		log.Fatalf("Unsupported log level %d", level)
	}

	return &logger{
		level: level,
	}
}

func NewStructuredStdoutLogger() *slog.Logger {
	return slog.New(slog.HandlerOptions{
		Level: slog.LevelInfo,
	}.NewTextHandler(os.Stdout))
}

func LogLevelFromString(s string) int64 {
	s = strings.ToLower(s)
	switch s {
	case "debug":
		return LEVEL_DEBUG
	case "info":
		return LEVEL_INFO
	case "warn":
		return LEVEL_WARN
	case "error":
		return LEVEL_ERROR
	case "fatal":
		return LEVEL_FATAL
	default:
		return LEVEL_INFO
	}
}

type logger struct {
	level int64
}

func (l *logger) Debug(format string, v ...any) {
	if l.level >= LEVEL_DEBUG {
		log.Printf(format, v...)
	}
}

func (l *logger) Info(format string, v ...any) {
	if l.level >= LEVEL_INFO {
		log.Printf(format, v...)
	}
}

func (l *logger) Warn(format string, v ...any) {
	if l.level >= LEVEL_WARN {
		log.Printf(format, v...)
	}
}

func (l *logger) Error(format string, v ...any) {
	if l.level >= LEVEL_ERROR {
		log.Printf(format, v...)
	}
}

func (l *logger) Fatal(format string, v ...any) {
	log.Fatalf(format, v...)
}
