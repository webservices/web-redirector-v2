package models

import (
	"fmt"
	"math/rand"
	"net/url"
	"regexp"
	"strings"
	"time"

	"webredirector/util"
)

// from https://stackoverflow.com/a/31832326
const randomCharacterSet = "abcdefghijklmnopqrstuvwxyz23456789" // omit 1 and 0 for readability
func GenerateRandomSlug() string {
	b := make([]byte, suggestedSlugLength)
	for i := range b {
		b[i] = randomCharacterSet[rand.Int63()%int64(len(randomCharacterSet))]
	}
	return string(b)
}

// default slugs for testing
// should be overwritten when initializing the application in main
var ReservedSlugs = []string{"invision", "sticky", "destiny", "generous", "madness"}

// The number of characters for auto-generated slugs
var suggestedSlugLength = 5

// The mininum number of characters for any slug
var minSlugLength = 3

// The maximum number of characters for the `owner` field
// According to the Authz team, 255 is currently the maximum number of characters for all types of accounts
var maxOwnerLength = 255

// The maximum number of characters for any field (targetUrl, description etc.)
var maxFieldLength = 16000

// Note: we could fold all variables in this file into a single "models.Config" struct
// and initialize it with default values or from environment variables.
// Since for now all variables all well contained in this file and it's unlikely that
// we regularly need to change them, we'll keep it simple & stupid.

type RedirectEntry struct {
	Id                string
	Slug              string
	TargetUrl         string
	CreationTimestamp time.Time
	DeletionTimestamp time.Time
	OwnerId           string // OIDC sub (subject) claim
	Description       string
	AppendQuery       bool
	AccessCount       uint64
	AccessTimestamp   time.Time
	MatomoId          string
}

func (e *RedirectEntry) Delete() {
	e.DeletionTimestamp = time.Now()
}

func (e RedirectEntry) IsDeleted() bool {
	return !e.DeletionTimestamp.IsZero()
}

func (e *RedirectEntry) Update(targetUrl string, description *string, owner string, matomoId *string) []error {
	if targetUrl != "" {
		e.TargetUrl = targetUrl
	}
	if description != nil {
		e.Description = *description
	}
	if owner != "" {
		e.OwnerId = owner
	}

	if matomoId != nil {
		e.MatomoId = *matomoId
	}

	return e.Validate(false)
}

// Validate checks that the parameters of the model have sensible and permitted values
// Under certain circumstances, the validation of the slug parameter may be skipped:
// this is intended to allow exisiting entries to continue being valid.
func (e *RedirectEntry) Validate(validateSlug bool) []error {
	// simple implementation of multi-error in Go
	var errors []error
	adderror := func(format string, fields ...any) {
		errors = append(errors, fmt.Errorf(format, fields...))
	}

	// check slug
	if validateSlug {
		if util.StringInSlice(strings.ToLower(e.Slug), ReservedSlugs) {
			adderror("slug cannot be used")
		}
		if len(e.Slug) < minSlugLength {
			adderror("slug is too short (minimum length: %d characters)", minSlugLength)
		}
		// allowed characters: a-z  0-9 . -
		if !regexp.MustCompile(`^([a-zA-Z0-9\.\-])+$`).MatchString(e.Slug) {
			adderror("slug must consist of alphanumeric characters, dash (-) and dot (.) signs")
		}
	}

	// check target url
	if len(e.TargetUrl) > maxFieldLength {
		adderror("target url exceeds %d characters", maxFieldLength)
	}
	// parse the URL with Golang's stdlib to ensure the incoming urls are valid
	// (and cannot be used for weird injection or cross-site-scripting attacks)
	// https://godocs.io/net/url#URL
	parsedUrl, err := url.Parse(e.TargetUrl)
	if err != nil {
		adderror("invalid target url: %s", err)
	} else {
		if parsedUrl.Scheme != "http" && parsedUrl.Scheme != "https" && parsedUrl.Scheme != "" {
			adderror("invalid target url: unsupported protocol")
		} else if parsedUrl.Hostname() == "" {
			adderror("invalid target url: must be absolute, not relative")
		} else {
			e.TargetUrl = parsedUrl.String()
		}
	}

	// check description
	if len(e.Description) > maxFieldLength {
		adderror("description exceeds %d characters", maxFieldLength)
	}

	// check owner
	if e.OwnerId == "" {
		adderror("owner is not set")
	}
	if len(e.OwnerId) > maxOwnerLength {
		// something probably went wrong here
		adderror("owner exceeds %d characters", maxOwnerLength)
	}

	// check matomoId, allowed characters: empty string or 0-9
	if !regexp.MustCompile(`^$|[0-9]`).MatchString(e.MatomoId) {
		adderror("matomo id must consist of numeric characters")
	}

	return errors
}

func NewShortUrlEntry(slug string, targetUrl string, description string, owner string) (RedirectEntry, []error) {
	if slug == "" {
		slug = GenerateRandomSlug()
	}
	e := RedirectEntry{
		Slug:              strings.ToLower(slug),
		TargetUrl:         targetUrl,
		CreationTimestamp: time.Now(),
		DeletionTimestamp: time.Time{}, // zero value
		OwnerId:           owner,
		Description:       description,
		AppendQuery:       false,
	}

	err := e.Validate(true)
	return e, err
}

type AuditRecord struct {
	Action          string
	AuditId         string
	Data            []byte
	RedirectEntryId string
	RequestId       string
	SourceIP        string
	Timestamp       time.Time
	User            string
}
