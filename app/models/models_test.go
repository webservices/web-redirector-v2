package models

import (
	"strings"
	"testing"
	"time"
)

func TestValidate(t *testing.T) {
	testCases := []struct {
		inputModel    RedirectEntry
		expectedError string
	}{
		{
			RedirectEntry{
				Slug:              "foo-bar.123",
				TargetUrl:         "https://cern.ch/security",
				CreationTimestamp: time.Now(),
				OwnerId:           "valid",
				Description:       "",
				AppendQuery:       false,
			}, "", // this is a valid entry, we don't expect any error
		},
		{
			RedirectEntry{
				Slug:              "asdjasdasda-asdkjasd.adasdA023e3d9..",
				TargetUrl:         "https://atlas.cern/" + GenerateRandomSlug(),
				CreationTimestamp: time.Now(),
				OwnerId:           "askdj90",
				Description:       strings.Repeat("ABC-123", 500),
				AppendQuery:       true,
			}, "", // also a valid entry
		},
		{
			RedirectEntry{
				Slug:              "non^valid/slug",
				TargetUrl:         "http://www.cern.ch",
				CreationTimestamp: time.Now(),
				OwnerId:           "valid",
				Description:       "",
				AppendQuery:       true,
			}, "slug must consist of alphanumeric characters",
		},
		{
			RedirectEntry{
				Slug:              "valid--slug",
				TargetUrl:         "http://www.cern.ch",
				CreationTimestamp: time.Now(),
				OwnerId:           "",
				Description:       "",
				AppendQuery:       false,
			}, "owner is not set",
		},
		{
			RedirectEntry{
				Slug:              "valid--slug",
				TargetUrl:         "http://www.cern.ch",
				CreationTimestamp: time.Now(),
				OwnerId:           strings.Repeat("a", 300),
				Description:       "",
				AppendQuery:       false,
			}, "owner exceeds",
		},
		{
			RedirectEntry{
				Slug:              "valid--slug",
				TargetUrl:         "https://home.web.cern.ch",
				CreationTimestamp: time.Now(),
				OwnerId:           "valid",
				Description:       strings.Repeat("x", 17000),
				AppendQuery:       false,
			}, "description exceeds",
		},
		{
			RedirectEntry{
				Slug:              "-.",
				TargetUrl:         "http://www.cern.ch",
				CreationTimestamp: time.Now(),
				OwnerId:           "valid",
				Description:       "",
				AppendQuery:       false,
			}, "slug is too short",
		},
		{
			RedirectEntry{
				Slug:              "foobar",
				TargetUrl:         "http://home.cern/" + strings.Repeat("a", 17000),
				CreationTimestamp: time.Now(),
				OwnerId:           "valid",
			}, "target url exceeds",
		},
		{
			RedirectEntry{
				Slug:              "invision",
				TargetUrl:         "http://www.cern.ch",
				CreationTimestamp: time.Now(),
				OwnerId:           "valid",
			}, "slug cannot be used",
		},
		{
			RedirectEntry{
				Slug:              "ab",
				TargetUrl:         "http://cern.ch",
				CreationTimestamp: time.Now(),
				OwnerId:           "valid",
			}, "slug is too short",
		},
		{
			RedirectEntry{
				Slug:              "ab",
				TargetUrl:         "http://cern.ch",
				CreationTimestamp: time.Now(),
				OwnerId:           "valid",
			}, "slug is too short",
		},
		{
			RedirectEntry{
				Slug:              "abc123",
				TargetUrl:         "http:///foo",
				CreationTimestamp: time.Now(),
				OwnerId:           "valid",
			}, "invalid target url",
		},
		{
			RedirectEntry{
				Slug:              "abc123",
				TargetUrl:         "example.com/foo/",
				CreationTimestamp: time.Now(),
				OwnerId:           "valid",
			}, "invalid target url",
		},
		{
			RedirectEntry{
				Slug:              "abc123",
				TargetUrl:         "http://:3333/",
				CreationTimestamp: time.Now(),
				OwnerId:           "valid",
			}, "invalid target url",
		},
	}
	for _, tc := range testCases {
		errors := tc.inputModel.Validate(true)
		errorMessages := ""
		for _, err := range errors {
			errorMessages += " " + err.Error()
		}
		if tc.expectedError == "" && errorMessages != "" {
			t.Errorf("Expected no error, got '%s': %+v", errorMessages, tc.inputModel)
		} else {
			if !strings.Contains(errorMessages, tc.expectedError) {
				t.Errorf("Expected error '%s', got '%s': %+v", tc.expectedError, errorMessages, tc.inputModel)
			}
		}

	}
}

func TestNewShortUrlEntry(t *testing.T) {
	e, errs := NewShortUrlEntry("My-Slug", "http://localhost:8080", "description", "owner")
	assert(t, len(errs), 0)
	assert(t, e.Slug, "my-slug") // all slugs should be lower-cased to make them case insensitive
	assert(t, e.OwnerId, "owner")
	assert(t, e.Description, "description")
	assert(t, e.AppendQuery, false)

	e, errs = NewShortUrlEntry("", "http://localhost:8080", "description", "owner")
	assert(t, len(errs), 0)
	assert(t, len(e.Slug), suggestedSlugLength)
}

func TestUpdate(t *testing.T) {
	e, errs := NewShortUrlEntry("old-slug", "http://localhost:8080", "old-description", "old-owner")
	assert(t, len(errs), 0)
	e.Update("http://cern.ch", nil, "new-owner", nil)
	assert(t, e.TargetUrl, "http://cern.ch")
	assert(t, e.Description, "old-description")
	assert(t, e.OwnerId, "new-owner")
	assert(t, e.MatomoId, "")
}

func assert(t *testing.T, got, want any) {
	if got != want {
		t.Fatalf("Expected: %v, got: %v\n", want, got)
	}
}
