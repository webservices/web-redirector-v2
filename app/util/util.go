package util

import (
	"bufio"
	"context"
	"fmt"
	"os"
	"os/signal"
	"runtime/pprof"
	"strconv"
	"strings"
	"syscall"
)

func GetEnv(key string, defaultValue string, required bool) string {
	val := os.Getenv(key)
	if val == "" {
		if required {
			panic(fmt.Sprintf("Environment variable %s is required but was not found (or is empty)", key))
		}
		val = defaultValue
	}

	return val
}

func GetEnvInt(key string, defaultValue string, required bool) int {
	valRaw := GetEnv(key, defaultValue, required)
	val, err := strconv.Atoi(valRaw)
	if err != nil {
		panic(fmt.Sprintf("Failed to convert environment variable '%s=%s' to int: %s", key, valRaw, err))
	}
	return val
}

func GetEnvSlice(key string, defaultValue string, required bool) []string {
	val := GetEnv(key, defaultValue, required)
	if len(val) > 0 {
		return strings.Split(val, ",")
	}
	return []string{defaultValue}
}

func EnsureTrailingSlash(url string) string {
	if !strings.HasSuffix(url, "/") {
		url = url + "/"
	}
	return url
}

// extrated from https://github.com/emirozer/go-helpers/
func StringInSlice(str string, slice []string) bool {
	for _, item := range slice {
		if item == str {
			return true
		}
	}
	return false
}

func StringSliceEquals(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}

	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

func ParseReservedSlugs(fileName string) ([]string, error) {
	reservedSlugs := []string{}
	file, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		// ignore comments
		if strings.HasPrefix(line, "#") {
			continue
		}
		reservedSlugs = append(reservedSlugs, strings.ToLower(line))
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}
	return reservedSlugs, nil
}

// Cancels the given context when the application receives SIGTERM (Ctrl-C)
func ContextWithSigterm(ctx context.Context) context.Context {
	// create a new, cancel-able context
	ctxWithCancel, cancel := context.WithCancel(ctx)

	// add labels to the go routine which is waiting for the signal
	// https://rakyll.org/profiler-labels/
	pprof.Do(ctx, pprof.Labels("wait-for-signal", "sigterm"),
		func(ctx context.Context) {
			go func() {
				defer cancel()

				signalCh := make(chan os.Signal, 1)
				signal.Notify(signalCh, os.Interrupt, syscall.SIGTERM)

				select {
				case <-signalCh:
				case <-ctx.Done():
				}
			}()
		},
	)

	return ctxWithCancel
}
