package util

import (
	"fmt"
	"testing"
)

func TestStringInSlice(t *testing.T) {
	var slice = []string{"foo", "bar", "baz"}
	var got bool

	got = StringInSlice("bar", slice)
	if got != true {
		t.Fatalf("StringInSlice returned %t, expected true: %v", got, slice)
	}

	got = StringInSlice("foobar", slice)
	if got != false {
		t.Fatalf("StringInSlice returned %t, expected false: %v", got, slice)
	}
}

func TestGetEnv(t *testing.T) {
	var val string
	// environment variable should be unset now
	const envVar = "SOME_LONG_ENV_VAR_NAME"
	const defaultVal = "Hello, world!"
	const customVal = "Bonjour!"

	// unset environment variable should get us the default value if it's not required
	val = GetEnv(envVar, defaultVal, false)
	if val != defaultVal {
		t.Fatalf("GetEnv returned '%s', expected '%s'", val, defaultVal)
	}

	// env var is required now, function should panic
	shouldPanic(t, func() { GetEnv(envVar, "", true) })

	// env var is now set
	t.Setenv(envVar, customVal)
	val = GetEnv(envVar, "", true)
	if val != customVal {
		t.Fatalf("GetEnv returned '%s', expected '%s'", val, defaultVal)
	}
}

func shouldPanic(t *testing.T, f func()) {
	defer func() { recover() }()
	f()
	t.Errorf("should have panicked")
}

func TestGetEnvInt(t *testing.T) {
	var val int
	const envVar = "MY_STRING_INT_ENV_VAR"
	const defaultVal = 0
	const customVal = -42

	val = GetEnvInt(envVar, fmt.Sprintf("%d", defaultVal), false)
	if val != defaultVal {
		t.Fatalf("GetEnv returned '%d', expected '%d'", val, defaultVal)
	}

	t.Setenv(envVar, fmt.Sprintf("%d", customVal))
	val = GetEnvInt(envVar, fmt.Sprintf("%d", defaultVal), false)
	if val != customVal {
		t.Fatalf("GetEnv returned '%d', expected '%d'", val, customVal)
	}

	t.Setenv(envVar, "this-is-not-an-int")
	shouldPanic(t, func() { GetEnvInt(envVar, "", true) })
}

func TestEnsureTrailingSlash(t *testing.T) {
	var input, want, got string

	input = "https://example.com"
	want = "https://example.com/"
	got = EnsureTrailingSlash(input)
	if got != want {
		t.Fatalf("EnsureTrailingSlash returned '%s', expected '%s'", got, want)
	}

	input = "https://example.com/"
	want = "https://example.com/"
	got = EnsureTrailingSlash(input)
	if got != want {
		t.Fatalf("EnsureTrailingSlash returned '%s', expected '%s'", got, want)
	}
}

func TestGetEnvSlice(t *testing.T) {
	var val []string
	// environment variable should be unset now
	const envVar = "AN_ENV_VAR_WITH_COMMA_SEPARATED_VALUES"
	var defaultValStr = "one,two,three"
	var defaultVal = []string{"one", "two", "three"}
	var customValStr = "a,b,"
	var customVal = []string{"a", "b", ""}

	// unset environment variable should get us the default value if it's not required
	val = GetEnvSlice(envVar, defaultValStr, false)
	if !StringSliceEquals(val, defaultVal) {
		t.Fatalf("GetEnvSlice returned '%v', expected '%v'", val, defaultVal)
	}

	// env var is required now, function should panic
	shouldPanic(t, func() { GetEnv(envVar, "", true) })

	// env var is now set
	t.Setenv(envVar, customValStr)
	val = GetEnvSlice(envVar, "", true)
	if !StringSliceEquals(val, customVal) {
		t.Fatalf("GetEnvSlice returned '%v', expected '%v'", val, defaultVal)
	}
}

func TestStringSliceEquals(t *testing.T) {
	if !StringSliceEquals([]string{"foo", "bar"}, []string{"foo", "bar"}) {
		t.Errorf("Returned false, expected true")
	}
	if StringSliceEquals([]string{"bar", "foo"}, []string{"foo", "bar"}) {
		t.Errorf("Returned true, expected false")
	}
	if StringSliceEquals([]string{"bar"}, []string{"baz", "baz"}) {
		t.Errorf("Returned true, expected false")
	}
}

func TestParseReservedSlugs(t *testing.T) {
	expectedReservedSlugs := []string{"invision", "sticky", "destiny", "generous", "madness", "emacs", "climb", "blowing", "fascinating", "landscapes", "heated", "lafayette", "jackie", "computation", "cardiovascular", "sparc", "cardiac", "salvation", "promotion", "watershed"}
	reservedSlugs, _ := ParseReservedSlugs("../hack/reserved_slugs.txt")
	fmt.Print(reservedSlugs)
	if len(reservedSlugs) != 20 {
		t.Errorf("ParseReservedSlugs returned '%v', expected '%v'", reservedSlugs, expectedReservedSlugs)
	}
}
