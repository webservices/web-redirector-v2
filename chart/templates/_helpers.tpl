{{/*
This set up doesn't support strings with special characters out of the box
in case this is necessary refer to https://stackoverflow.com/a/3177871.
NB: Special character either in the username or in the password are not recommended
even by the database team.
*/}}

{{- define "postgres.connection-string" -}}
{{- printf "postgresql://%s:%s@%s:%s/%s" .Values.env.postgresDatabase.user .Values.env.postgresDatabase.password .Values.env.postgresDatabase.host .Values.env.postgresDatabase.port .Values.env.postgresDatabase.database | b64enc -}}
{{- end -}}
