{{- if .Values.logCollector.enabled }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Release.Name }}-log-collector
  labels:
    app: {{ .Release.Name }}
    component: log-collector
spec:
  # Only one instance of Vector must run at any time
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: {{ .Release.Name }}
      component: log-collector
  template:
    metadata:
      labels:
        app: {{ .Release.Name }}
        component: log-collector
      annotations:
        # Automatically restart pod when config changes
        checksum/config: {{ include (print $.Template.BasePath "/vector-config.yaml") . | sha256sum }}
    spec:
      containers:
      - name: vector
        image: {{ .Values.logCollector.image | quote }}
        imagePullPolicy: {{ .Values.imagePullPolicy }}
        args:
          - "--config-dir"
          - "/etc/vector"
          - "--require-healthy"
          - "true"
        ports:
        - name: syslog
          containerPort: 50514
          protocol: UDP
        - name: api
          containerPort: 8686
          protocol: TCP
        volumeMounts:
        - name: config
          mountPath: /etc/vector
        - name: data
          mountPath: /var/lib/vector
        - name: cern-ca
          mountPath: "/etc/ssl/certs/CERN-Root-Certification-Authority-2.crt"
          subPath: "CERN-Root-Certification-Authority-2.crt"
        livenessProbe:
          httpGet:
            path: /health
            port: api
        readinessProbe:
          httpGet:
            path: /health
            port: api
      volumes:
        - name: config
          secret:
            secretName: {{ .Release.Name }}-vector-config
        - name: data
          {{- if .Values.logCollector.persistence.enabled }}
          persistentVolumeClaim:
            claimName: {{ .Release.Name }}-logs
          {{- else }}
          emptyDir: {}
          {{- end }}
        - name: cern-ca
          configMap:
            name: {{ .Release.Name }}-cern-ca-root-certificate
---
apiVersion: v1
kind: Service
metadata:
  name: {{ .Release.Name }}-log-collector
  labels:
    app: {{ .Release.Name }}
    component: log-collector
spec:
  ports:
    - protocol: UDP
      port: 514
      targetPort: syslog
  selector:
    app: {{ .Release.Name }}
    component: log-collector
---
{{- if .Values.logCollector.persistence.enabled }}
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: {{ .Release.Name }}-logs
  labels:
    app: {{ .Release.Name }}
    component: log-collector
spec:
  storageClassName: {{ .Values.logCollector.persistence.storageClass | quote }}
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: {{ .Values.logCollector.persistence.size | quote }}
{{- end }}
{{- end }}
